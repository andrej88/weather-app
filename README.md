# README #

This is a free and open-parcel weather app for Android.

In time, it will feature weather forecasts, weather history, sea conditions (tides, currents, water temperature), sky conditions (cloud cover, seeing), aurora forecasts, radar/satellite maps, and more.

[Outline](https://docs.google.com/document/d/15NnTXCExxIiSIm_49r454BcNNR3Pchhs-FFEyCh-mQA/edit?usp=sharing)

Contact 9andrej6@gmail.com