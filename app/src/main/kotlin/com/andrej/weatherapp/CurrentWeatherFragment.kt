package com.andrej.weatherapp

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.properties.PropertyDataSets.CurrentWeatherProperties
import com.andrej.weatherapp.properties.ScalarQuantities.*
import com.andrej.weatherapp.providers.CurrentWeatherProvider
import com.andrej.weatherapp.util.imageViewCompat
import com.andrej.weatherapp.util.pairEmUpRemainder
import org.jetbrains.anko.*
import java.text.SimpleDateFormat

class CurrentWeatherFragment() : WeatherFragment() {

    lateinit private var parser: ConditionParser

    lateinit var provider: CurrentWeatherProvider
    private val providerKey = CurrentWeatherProvider::class.java.canonicalName
    private var previousData: CurrentWeatherProperties? = null

    private lateinit var location: LatLong
    private val locationKey = LatLong::class.java.canonicalName

    lateinit private var dataUnavailable: String
    lateinit private var noNewData: String
    lateinit private var yesNewData: String

    lateinit private var progressBar: ProgressBar
    lateinit private var weatherInfoLayout: LinearLayout
    lateinit private var updateTime: TextView

    lateinit private var weatherConditionImage: ImageView
    lateinit private var weatherConditionLabel: TextView
    lateinit private var weatherConditionList: LinearLayout

    constructor(location: LatLong) : this() {
        this.location = location
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            provider = savedInstanceState.get(providerKey) as CurrentWeatherProvider
            location = savedInstanceState.get(locationKey) as LatLong
        }
        parser = ConditionParser(resources)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataUnavailable = resources.getString(R.string.data_unavailable)
        noNewData = resources.getString(R.string.no_new_current_data_found)
        yesNewData = resources.getString(R.string.new_current_data_found)

        refreshData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(providerKey, provider)
        outState.putParcelable(locationKey, location)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = WeatherFragmentUi().createView(AnkoContext.create(activity, this))

    override fun refreshData(userTriggered: Boolean) {

        progressBar.visibility = View.VISIBLE
        weatherInfoLayout.visibility = View.GONE
        updateTime.visibility = View.GONE

        doAsync {

            val data = provider.acquire(location)

            uiThread {

                if (previousData == data && userTriggered) {
                    activity.toast(noNewData)
                }
                else {

                    if (userTriggered) {
                        activity.toast(yesNewData)
                    }

                    previousData = data

                    weatherConditionImage.image = parser.getConditionIcon(data.textSummary?.shortest)
                    weatherConditionLabel.text = data.textSummary?.shortest ?: dataUnavailable

                    weatherConditionList.removeAllViews()

                    val quantityViews = mutableListOf<QuantityView>()

                    data.temperature?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_temperature,
                                   it.temperature?.format(Temperature.Units.CELSIUS, "%.1f", true)
                                   ?: dataUnavailable)
                        quantityViews.add(view)
                    }

                    data.dewPoint?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_temperature_dew_point,
                                   it.measure?.format(Temperature.Units.CELSIUS, "%.1f", true)
                                   ?: dataUnavailable)
                        quantityViews.add(view)
                    }

                    data.windChill?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_temperature_wind_chill,
                                   it.measure?.format(WindChill.Units.CELSIUS, "%.0f", true)
                                   ?: dataUnavailable)
                        quantityViews.add(view)
                    }

                    data.pressure?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_gauge,
                                   it.measure?.format(Pressure.Units.MILLIBAR, "%.0f", true)
                                   ?: dataUnavailable)
                        quantityViews.add(view)
                    }

                    data.visibility?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_eye,
                                   it.measure?.format(Distance.Units.KILOMETER, "%.1f", true)
                                   ?: dataUnavailable)
                        quantityViews.add(view)
                    }

                    data.humidity?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_humidity,
                                   it.measure?.format(Percentage.Units.PERCENT, "%.0f", true)
                                   ?: dataUnavailable)
                        quantityViews.add(view)
                    }

                    data.wind?.let {
                        val view = QuantityView(context)
                        view.setup(R.drawable.quantity_wind_direction,
                                   it.speed?.format(Speed.Units.KMH, "%.0f", true)
                                   ?: "Speed Unknown") {
                            it.direction?.let {
                                rotation = it.inUnit(Direction.Units.DEGREES).toFloat() + 180f
                            }
                        }
                        quantityViews.add(view)
                    }

                    val quantityViewsPaired = quantityViews.pairEmUpRemainder()

                    with(weatherConditionList) {

                        quantityViewsPaired.first.forEach { pair ->
                            linearLayout {

                                addView(pair.first)
                                addView(pair.second)

                                lparams(width = matchParent, height = wrapContent) {
                                    bottomMargin = dip(8f)
                                    topMargin = dip(8f)
                                }
                            }
                        }

                        quantityViewsPaired.second?.let {
                            linearLayout {

                                addView(quantityViewsPaired.second)

                                lparams(width = matchParent, height = wrapContent) {
                                    bottomMargin = dip(8f)
                                    topMargin = dip(8f)
                                }
                            }
                        }
                    }

                    updateTime.text = SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.LONG,
                                                                           SimpleDateFormat.SHORT).format(data.time?.time)
                }
                progressBar.visibility = View.GONE
                weatherInfoLayout.visibility = View.VISIBLE
                updateTime.visibility = View.VISIBLE
            }
        }
    }

    private inner class WeatherFragmentUi : AnkoComponent<Fragment> {
        override fun createView(ui: AnkoContext<Fragment>): View = with(ui) {
            scrollView {
                lparams {
                    height = wrapContent
                    width = matchParent
                }

                verticalLayout {

                    progressBar = progressBar {
                        visibility = View.GONE
                    }.lparams(width = dip(100f), height = dip(100f)) {
                        topMargin = dip(50f)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }

                    weatherInfoLayout = verticalLayout {

                        weatherConditionImage = imageViewCompat {

                            setImageResource(R.drawable.condition_unknown)

                        }.lparams(width = matchParent, height = dip(128f)) {
                            // Do not set gravity in the parameters, because that will call the wrong
                            // lparams method and cause a ClassCastException.
                            bottomMargin = dip(12f)
                            topMargin = dip(32f)
                            gravity = Gravity.CENTER
                        }

                        weatherConditionLabel = textView {

                            // http://stackoverflow.com/questions/9877946/text-size-and-different-android-screen-sizes
                            textSize = dip(24f).toFloat()

                            @Suppress("DEPRECATION")
                            setTextColor(resources.getColor(R.color.textLight))

                            gravity = Gravity.CENTER

                        }.lparams(width = matchParent, height = wrapContent) {
                            bottomMargin = dip(32f)
                            gravity = Gravity.CENTER
                        }

                        weatherConditionList = verticalLayout()

                    }.lparams(width = matchParent, height = wrapContent) {
                        bottomMargin = dip(16f)
                    }

                    updateTime = textView {
                        textSize = sp(16f).toFloat()

                        @Suppress("DEPRECATION")
                        setTextColor(resources.getColor(R.color.textLight))
                    }.lparams {
                        leftMargin = dip(16f)
                        rightMargin = dip(16f)
                    }
                }
            }
        }
    }

    private class QuantityView(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
        : LinearLayout(context, attrs, defStyleAttr) {

        val quantityIconSize = 32f

        constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
        constructor(context: Context) : this(context, null)

        fun setup(iconResId: Int, text: String, formatIcon: (ImageView.() -> Unit)? = null) {

            val iconView = AppCompatImageView(context)
            iconView.imageResource = iconResId
            val iconLayoutParams = LinearLayout.LayoutParams(dip(quantityIconSize), dip(quantityIconSize))
            iconLayoutParams.gravity = Gravity.CENTER
            iconLayoutParams.leftMargin = dip(16f)
            iconLayoutParams.rightMargin = dip(8f)
            iconView.layoutParams = iconLayoutParams
            formatIcon?.let { iconView.it() }
            addView(iconView)

            val labelView = TextView(context)
            labelView.textSize = dip(18f).toFloat()

            @Suppress("DEPRECATION")
            labelView.textColor = resources.getColor(R.color.textLight)

            labelView.text = text
            val textLayoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
            textLayoutParams.gravity = Gravity.CENTER_VERTICAL
            labelView.layoutParams = textLayoutParams
            addView(labelView)

            val thisLayoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
            thisLayoutParams.weight = 1.0f
            layoutParams = thisLayoutParams
        }
    }
}