package com.andrej.weatherapp.util

/**
 * Returns a list of all elements in the receiving array, but where adjacent elements are paired
 * up. If there is an odd number of elements, the last element is returned as a remainder.
 *
 * Examples:
 *
 * { 1  4  6  2  3 }  ->  ( { (1  4)  (6  2) }, 3 )
 *
 * { 2  9  2  0 }     ->  ( { (2  9)  (2  0) }, null)
 *
 */
fun <T> List<T>.pairEmUpRemainder(): Pair<List<Pair<T, T>>, T?> =
        if (count() % 2 == 0) {
            Pair(pairEmUp(), null as T?)
        } else {
            Pair(subList(0, count() - 1).pairEmUp(), this[count() - 1])
        }

/**
 * Just like [pairEmUpRemainder], but only a list is returned - no remainder.
 *
 * Examples:
 *
 * { 1  4  6  2  3 }  ->  { (1  4)  (6  2) }
 *
 * { 2  9  2  0 }     ->  { (2  9)  (2  0) }
 *
 */
fun <T> List<T>.pairEmUp(): List<Pair<T, T>> {
    val startOfLastPair =
            if (count() % 2 == 0) {
                count() - 2
            } else {
                count() - 3
            }
    return (0..startOfLastPair step 2).map { Pair(this[it], this[it + 1]) }
}

fun <T> MutableCollection<T>.addMissing(toAdd: Collection<T>) {
    toAdd.forEach { if (!contains(it)) add(it) }
}

fun <T> MutableCollection<T>.addIfMissing(toAdd: T) {
    if (!contains(toAdd)) add(toAdd)
}

infix fun String.says(other: String): Boolean {
    val trimChars = charArrayOf(' ', '.', ',', '"', '\'')
    return this.trim(*trimChars).equals(other.trim(*trimChars), true)
}