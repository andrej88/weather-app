package com.andrej.weatherapp.util

import org.w3c.dom.Element
import org.w3c.dom.Node

val Node.childNodesIterable: Iterable<Node>
    get() {
        val nodeList = childNodes
        return (1..nodeList.length).map { nodeList.item(it) }
    }

val Node.childElements: Iterable<Element>
    get() = childNodesIterable.filterIsInstance(Element::class.java)

/**
 * Returns a child node with the name [childName]
 *
 * If multiple such nodes exist, only one of them is returned.
 */
operator fun Node.get(childName: String) = childElements.find { it.nodeName == childName }

/**
 * Returns a child node with the specified name and attribute values.
 *
 * If multiple child nodes satisfy the conditions, only one is returned.
 */
fun Node.get(childName: String, attributes: Map<String, String>) = childElements.find { e -> e.nodeName == childName && attributes.all { e.getAttribute(it.key) == it.value } }

/**
 * Returns all child nodes with the name [childName]
 */
fun Node.getAll(childName: String) = childElements.filter { it.nodeName == childName }

/**
 * Reworks the attribute NamedNodeMap into a normal Map
 *
 * @param useCached if there is a cached map, it is returned so as to avoid unnecessarily making maps.
 */
fun Element.attributeMap(useCached: Boolean = false): Map<String, String> {
    val cached = attributeMapCache[this]
    if (cached != null && useCached) {
        return cached
    }
    else {
        attributeMapCache.remove(this)
        val map = mutableMapOf<String, String>()
        for (i in 0 until attributes.length) {
            map.put(attributes.item(i).nodeName, attributes.item(i).textContent)
        }
        attributeMapCache.put(this, map)
        return map
    }
}
private val attributeMapCache = mutableMapOf<Element, Map<String, String>>()


fun Element.getChildrenByName(name: String): List<Element> {
    val nodeList = getElementsByTagName(name)
    return (0 until nodeList.length).map { nodeList.item(it) as? Element }.filterNotNull()
}