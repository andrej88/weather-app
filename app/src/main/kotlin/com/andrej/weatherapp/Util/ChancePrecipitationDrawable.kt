package com.andrej.weatherapp.util

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.graphics.drawable.VectorDrawableCompat
import com.andrej.weatherapp.R

class ChancePrecipitationDrawable(resources: Resources, val precipitationDrawable: Drawable) : Drawable() {

    val chanceIconDrawable = VectorDrawableCompat.create(resources, R.drawable.condition_precipitation_chance_overlay, null)
                             ?: throw Exception("Failed to create chance drawable overlay for ChancePrecipitationDrawable")

    init {
        bounds = precipitationDrawable.bounds
    }

    override fun draw(canvas: Canvas?) {
        precipitationDrawable.draw(canvas)
        chanceIconDrawable.draw(canvas)
    }

    override fun onBoundsChange(bounds: Rect?) {
        precipitationDrawable.bounds = bounds
        chanceIconDrawable.bounds = bounds
    }

    override fun setAlpha(alpha: Int) = throw UnsupportedOperationException()
    override fun getOpacity() = throw UnsupportedOperationException()
    override fun setColorFilter(colorFilter: ColorFilter?) = throw UnsupportedOperationException()
}