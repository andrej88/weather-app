package com.andrej.weatherapp.util

import android.content.res.Resources
import android.support.graphics.drawable.VectorDrawableCompat
import com.andrej.weatherapp.properties.ScalarQuantities.Measurable

/**
 * Returns null if receiver is null or blank, else returns receiver as String
 */
fun CharSequence.stringIfNotBlank() = takeUnless { isNullOrBlank() }?.toString()

fun sin2(a: Double): Double {
    val b = Math.sin(a)
    return b * b
}

fun <T : Measurable.Units> parseUnit(symbol: String?, units: Array<T>) =
        units.find { it.symbols?.contains(symbol) ?: false }
        ?: throw Exception("Unknown unit: $symbol")

fun Resources.getVectorDrawable(resId: Int) = VectorDrawableCompat.create(this, resId, null)
                                              ?: throw Exception("Parsing error during VectorDrawableCompat creation, see log.")