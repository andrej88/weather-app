package com.andrej.weatherapp.util

import android.app.Activity
import android.content.Context
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.AppCompatImageView
import android.view.View
import android.view.ViewManager
import com.github.mikephil.charting.charts.LineChart
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.internals.AnkoInternals

/**
 * This is a fix for the Anko tabHost() functions, which initialize a TabHost using only the
 * TabHost(Context) constructor. That constructor bypasses the full constructor so some
 * initialization won't take place. The functions here call the TabHost(Context, AttributeSet)
 * constructor (with a null AttributeSet) so that all the required initialization takes place.
 *
 * Code taken from: https://github.com/Kotlin/anko/issues/274
 */
fun Activity.tabHostAttrSet(theme: Int = 0): android.widget.TabHost = tabHostAttrSet(theme) {}
inline fun Activity.tabHostAttrSet(theme: Int = 0, init: android.widget.TabHost.() -> Unit): android.widget.TabHost {
    return ankoView({ ctx: Context -> android.widget.TabHost(ctx, null) }, theme) { init() }
}

fun ViewManager.imageViewCompat(theme: Int = 0): AppCompatImageView = imageViewCompat(theme) {}
inline fun ViewManager.imageViewCompat(theme: Int = 0, init: AppCompatImageView.() -> Unit): AppCompatImageView {
    return ankoView({ ctx: Context -> AppCompatImageView(ctx, null) }, theme) { init() }
}

fun ViewManager.lineChart(theme: Int = 0) = lineChart(theme) {}
inline fun ViewManager.lineChart(theme: Int = 0, init: LineChart.() -> Unit) = ankoView(::LineChart, theme, init)

fun <T : View> TabLayout.ankoViewForTabLayout(factory: (ctx: Context) -> T, theme: Int, init: T.() -> Unit): T {
    val ctx = AnkoInternals.wrapContextIfNeeded(AnkoInternals.getContext(this), theme)
    val view = factory(ctx)
    view.init()
    addViewForTabLayout(this, view)
    return view
}

private fun <T : View> addViewForTabLayout(manager: TabLayout, view: T) {
    return when (view) {
        is ViewPager -> manager.setupWithViewPager(view)
        else -> manager.addView(view)
    }
}