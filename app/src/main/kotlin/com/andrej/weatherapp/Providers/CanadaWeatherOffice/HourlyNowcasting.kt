package com.andrej.weatherapp.providers.CanadaWeatherOffice

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.andrej.weatherapp.properties.*
import com.andrej.weatherapp.properties.PropertyDataSets.NowcastingGroup
import com.andrej.weatherapp.properties.PropertyDataSets.NowcastingProperties
import com.andrej.weatherapp.properties.Qualities.AccumulationSubstance
import com.andrej.weatherapp.properties.Qualities.PrecipitationType
import com.andrej.weatherapp.properties.Qualities.VisibilityCause
import com.andrej.weatherapp.properties.ScalarQuantities.*
import com.andrej.weatherapp.providers.NowcastingProvider
import com.andrej.weatherapp.util.addMissing
import com.andrej.weatherapp.util.stringIfNotBlank
import org.apache.commons.compress.compressors.CompressorStreamFactory
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class HourlyNowcasting(parcel: Parcel? = null) : NowcastingProvider {


    init {
        parcel?.let { unParcelSites(parcel) }
    }


    override fun acquire(location: LatLong): NowcastingGroup {

        // Download list of sites and get the csv file
        if (sites.isEmpty() ||
            System.currentTimeMillis() - sitesLastUpdatedTime >= howOftenShouldSitesUpdate) {
            if (areSitesBeingUpdated.compareAndSet(false, true)) {
                refreshSites()
                areSitesBeingUpdated.set(false)
            }
            else {
                while (areSitesBeingUpdated.get()) {
                }
            }
        }

        val closestSite = sites.minBy { (it.location distanceTo location) } ?: throw Exception("No weather sites available!")
        Log.d("CanadaNowcasting", "Code of closest nowcasting station: ${closestSite.id}")

        if ((closestSite.location distanceTo location) > minDistance) {
            throw Exception("No suitable location found")
        }


        // Download most recent nowcasting matrix page file

        val stream = URL(matricesFolderAddress).openStream()
        val readmeReader = BufferedReader(InputStreamReader(stream))

        var mostRecentFilename = ""

        readmeReader.useLines {
            it.forEach {
                if (it.startsWith("<img")) {
                    val foundPattern = Regex("""SCRIBE\.NWCSTG\.\d\d\.\d\d\.\d\dZ\.n\.Z""").find(it)
                    foundPattern?.let {
                        mostRecentFilename = it.value
                    }
                }
            }
        }

        stream.close()

        if (mostRecentFilename == "") {
            throw Exception("No nowcasting matrix file found")
        }
        else {
            Log.d("CanadaNowcasting", "Reading file $mostRecentFilename")
        }

        // How to get the most recent file:
        // 1) Look for the last file and download it.
        //    ... stupid and not very future-proof but it's the simplest solution.
        // 4) Unzip and parse the file.

        var calendarToBe: Calendar = Calendar.getInstance() // will change
        val listOfNowcasts = mutableListOf<NowcastingProperties>()

        // We have to decompress it
        val url = URL(matricesFolderAddress + mostRecentFilename)
        val bufferedInputStream = BufferedInputStream(url.openStream())
        val compressorStream = CompressorStreamFactory().createCompressorInputStream(bufferedInputStream)

        val reader = BufferedReader(InputStreamReader(compressorStream))
        reader.useLines {

            /**
             * Columns are:
             * 0  Timestamp
             * 1  Cloud Cover
             * 2  Cloud Ceiling
             * 3  Precipitation Type 1
             * 4  Precipitation Chance 1
             * 5  Precipitation Type 2
             * 6  Precipitation Chance 2
             * 7  Precipitation Type 3
             * 8  Precipitation Chance 3
             * 9  Total Chance of Precipitation
             * 10 Accumulation Quantity (CM for snow, MM for rain)
             * 11 Accumulation Type
             * 12 Temperature
             * 13 Dew Point
             * 14 Wind Direction
             * 15 Wind Velocity
             * 16 Wind Gust
             * 17 Visibility Distance
             * 18 Visibility Cause
             */
            val stationLineRegex = Regex("""STN:\s*${closestSite.id}.*""")
            var shouldRead = false
            Log.d("CanadaNowcasting", "Reading lines of nowcasting file")
            it.forEach {

                if (shouldRead && it.first().isDigit()) {
                    val columns = it.split('|').map(String::trim)
                    Log.d("CanadaNowcasting", "Reading line $columns")
                    listOfNowcasts.add(NowcastingProperties(
                            time = run {
                                val calendar = parseTimestamp(columns[0])
                                if (listOfNowcasts.size == 6) {
                                    calendarToBe = calendar
                                }
                                calendar
                            },

                            cloudCover = columns[1].stringIfNotBlank()?.let {
                                Cloudiness(measure = Percentage(columns[1].toDouble(),
                                                                Percentage.Units.TENTHS))
                            },

                            cloudCeiling = columns[2].stringIfNotBlank()?.takeIf { it != "999" }?.let {
                                CloudCeiling(measure = Distance(columns[2].toDouble() * 100,
                                                                Distance.Units.FEET))
                            },

                            precipitationChances = PrecipitationChances(
                                    precipitations =
                                    listOf(Pair(columns[3], columns[4].stringIfNotBlank()?.toDouble() ?: 0.0),
                                           Pair(columns[5], columns[6].stringIfNotBlank()?.toDouble() ?: 0.0),
                                           Pair(columns[7], columns[8].stringIfNotBlank()?.toDouble() ?: 0.0))
                                            .filter { it.first.isNotBlank() }
                                            .map {
                                                PrecipitationChances.WeightedPrecipitation(
                                                        type = PrecipitationType.get(it.first) ?: throw Exception("Unknown precipitation type: ${it.first}"),
                                                        weight = it.second)
                                            }.toSet()),

                            chanceOfPrecipitation = columns[9].stringIfNotBlank()?.let {
                                ChanceOfPrecipitation(measure = Percentage(columns[9].toDouble(),
                                                                           Percentage.Units.PERCENT))
                            },

                            accumulation = run {
                                val substance = AccumulationSubstance.get(columns[11])
                                PrecipitationAccumulation(
                                        accumulationSubstance = substance,
                                        measure =
                                        AccumulationAmount(amount = columns[10].toDouble(),
                                                           unit = if (substance == AccumulationSubstance.SNOW) {
                                                               AccumulationAmount.Units.CENTIMETER
                                                           }
                                                           else {
                                                               AccumulationAmount.Units.MILLIMETER
                                                           })
                                                              )
                            },

                            temperatureProperty = columns[12].stringIfNotBlank()?.let {
                                TemperatureProperty(temperature = Temperature(amount = columns[12].toDouble(),
                                                                              unit = Temperature.Units.CELSIUS))
                            },

                            dewPoint = columns[13].stringIfNotBlank()?.let {
                                DewPoint(measure = Temperature(amount = columns[13].toDouble(),
                                                               unit = Temperature.Units.CELSIUS))
                            },

                            wind = Wind(speed = Speed(amount = columns[15].toDouble(),
                                                      unit = Speed.Units.KMH),
                                        gust = columns[16].stringIfNotBlank()?.let {
                                            Speed(amount = columns[16].toDouble(),
                                                  unit = Speed.Units.KMH)
                                        },
                                        direction = Direction(amount = columns[14].toDouble(),
                                                              unit = Direction.Units.DEGREES)),

                            visibility = Visibility(measure = Distance(amount = columns[17].toDouble(),
                                                                       unit = Distance.Units.MILE),
                                                    cause = VisibilityCause.get(columns[18])))
                                      )
                }
                else if (shouldRead && it.first() == '.') {
                    Log.d("CanadaNowcasting", "Reached end of table")
                    reader.close()
                    return@useLines
                }
                else if (!shouldRead && it.matches(stationLineRegex)) {
                    Log.d("CanadaNowcasting", "Reading nowcast for $closestSite")
                    shouldRead = true
                }
            }
        }

        return NowcastingGroup(nowcasts = listOfNowcasts, location = location, time = calendarToBe)
    }


    private fun refreshSites() {

        val reader = BufferedReader(InputStreamReader(URL(readmeFileAddress).openStream()))

        val newSites = mutableListOf<Site>()


        var shouldRead = false
        reader.useLines {
            it.forEach {
                if (shouldRead) {
                    val columns = it.trim().split(Regex("""\s+"""))
                    newSites.add(Site(columns[0], columns[1], columns[2], LatLong(columns[3], columns[4]), columns[5]))
                }

                if (!shouldRead && it.matches(Regex("""ID\s+SYN\s+NOM\s+LAT\s+LON\s+ELEV"""))) {
                    shouldRead = true
                }
            }
        }

        sites.clear()
        sites.addAll(newSites)
        sitesLastUpdatedTime = System.currentTimeMillis()
    }


    private fun parseTimestamp(timestamp: String): Calendar {
        val instance = Calendar.getInstance()
        val dateFormatter = SimpleDateFormat("yyyyMMdd HHmm ZZZ", Locale.US)
        instance.time = dateFormatter.parse("$timestamp UTC")
        return instance
    }


    override fun writeToParcel(dest: Parcel, flags: Int) {
        packageSites(dest, flags)
    }

    companion object {

        /**
         * Multiple fragments (thus multiple threads) may be trying to access the list of sites at the same time.
         * If one thread is already refreshing the site list, the other should wait for it to finish.
         */
        private val areSitesBeingUpdated = AtomicBoolean(false)

        /**
         * We don't want to parcel/un-parcel the site list multiple times - once will do.
         * The Provider that parcels the site list in its own parcel should remember that,
         * and everyone else should wait for the site list to be un-parceled before trying
         * to do anything with it.
         */
        val areSitesParceled = AtomicBoolean(false)

        private var sitesLastUpdatedTime = 0L
        private const val howOftenShouldSitesUpdate = 86400000L

        /**
         * List of all available locations
         */
        private val sites = mutableListOf<Site>()

        /**
         * If there is no site closer than this distance (in meters)
         */
        private const val minDistance = 50.0e3

        fun packageSites(dest: Parcel, flags: Int) {
            if (areSitesParceled.compareAndSet(false, true)) {

                // Lets us know that we have saved the list of sites in *this* Provider's parcel.
                // Yes, this one. It's a big responsibility, and *this* Provider got chosen! Isn't that exciting?!
                // Ints instead of Booleans? What is this, C?
                dest.writeInt(1)

                // Ok now save the actual list of sites and its size.
                dest.writeInt(sites.size)
                dest.writeTypedArray(sites.toTypedArray(), flags)
            }
            else {
                dest.writeInt(0)
            }
        }

        fun unParcelSites(parcel: Parcel) {

            if (parcel.readInt() != 0 && areSitesParceled.get()) {
                val sitesArray = arrayOfNulls<Site>(parcel.readInt())
                parcel.readTypedArray(sitesArray, Site.CREATOR)
                sites.addMissing(sitesArray.filterNotNull().toList())
                areSitesParceled.set(false)
            }
        }

        private const val matricesFolderAddress = "http://dd.weatheroffice.ec.gc.ca/nowcasting/matrices/"
        private const val readmeFileAddress = "http://dd.weatheroffice.ec.gc.ca/nowcasting/doc/README_INCS-SIPI.txt"

        @JvmField @Suppress("unused")
        val CREATOR: Parcelable.Creator<HourlyNowcasting> = object : Parcelable.Creator<HourlyNowcasting> {

            override fun createFromParcel(source: Parcel) = HourlyNowcasting(source)

            override fun newArray(size: Int) = arrayOfNulls<HourlyNowcasting?>(size)
        }
    }

    data class Site(val id: String, val syn: String, val siteName: String,
                    val location: LatLong, val elevation: String) : Parcelable {

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeString(id)
            dest.writeString(syn)
            dest.writeString(siteName)
            dest.writeParcelable(location, flags)
            dest.writeString(elevation)
        }

        override fun describeContents() = 0

        companion object {
            @JvmField
            val CREATOR = object : Parcelable.Creator<Site> {

                override fun createFromParcel(source: Parcel) =
                        Site(source.readString(),
                             source.readString(),
                             source.readString(),
                             source.readParcelable(LatLong::class.java.classLoader),
                             source.readString())

                override fun newArray(size: Int): Array<out Site?> = arrayOfNulls(size)
            }
        }
    }
}