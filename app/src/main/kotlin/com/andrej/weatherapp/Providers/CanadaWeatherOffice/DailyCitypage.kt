package com.andrej.weatherapp.providers.CanadaWeatherOffice

import android.os.Parcel
import android.os.Parcelable
import com.andrej.weatherapp.properties.*
import com.andrej.weatherapp.properties.PropertyDataSets.ForecastGroup
import com.andrej.weatherapp.properties.PropertyDataSets.ForecastProperties
import com.andrej.weatherapp.properties.Qualities.AccumulationSubstance
import com.andrej.weatherapp.properties.Qualities.PrecipitationType
import com.andrej.weatherapp.properties.Qualities.VisibilityCause
import com.andrej.weatherapp.properties.ScalarQuantities.*
import com.andrej.weatherapp.providers.ForecastProvider
import com.andrej.weatherapp.util.*
import org.w3c.dom.Element

class DailyCitypage(parcel: Parcel? = null) : ForecastProvider {

    private var citypageProvider = CitypageWeatherProvider()

    init {
        parcel?.let {
            CitypageWeatherProvider.unParcelSites(it)
        }
    }

    override fun acquire(location: LatLong): ForecastGroup {
        val root = citypageProvider.downloadXmlAndFindDocumentRoot(location)


        val forecastGroup = root["forecastGroup"]

        return ForecastGroup(
                forecasts = forecastGroup?.getAll("forecast")?.map {

                    // Canada Weather's wind forecast direction says it's in degrees but the numbers don't match up with the cardinal direction. Looks like the number it shows is actually 1/10th of what it should be.

                    val textSummaryNode = it["textSummary"]
                    val abbreviatedForecastNode = it["abbreviatedForecast"]

                    ForecastProperties(
                            period = it["period"]?.let {
                                it.attributeMap()["textForecastName"]?.stringIfNotBlank()?.let(::RelativeDay)
                            },

                            textSummary = if (textSummaryNode != null || abbreviatedForecastNode != null) {
                                TextSummary(listOf(textSummaryNode?.textContent,
                                                   abbreviatedForecastNode?.get("textSummary")?.textContent).filterNotNull())
                            }
                            else {
                                null
                            },

                            temperatures = it["temperatures"]?.let { parseNodeOfTemperatures(it) },

                            chanceOfPrecipitation = run {
                                val popNode = abbreviatedForecastNode?.get("pop")
                                popNode?.textContent?.takeIf(String::isNotBlank)?.let {
                                    ChanceOfPrecipitation(measure = Percentage(popNode.textContent.toDouble(),
                                                                               parseUnit(popNode.attributeMap()["units"],
                                                                                         Percentage.Units.values())))
                                }
                            },

                            winds = it["winds"]?.let {
                                WindsWithSummary(textSummary = it["textSummary"]?.let { TextSummary(it.textContent) },
                                                 winds = it.getChildrenByName("wind").map {
                                                     Wind(
                                                             speed = run {
                                                                 val speedNode = it["speed"]
                                                                 speedNode?.textContent?.stringIfNotBlank()?.let {
                                                                     Speed(amount = it.toDouble(),
                                                                           unit = parseUnit(speedNode.attributeMap()["units"],
                                                                                            Speed.Units.values()))
                                                                 }
                                                             },

                                                             gust = run {
                                                                 val gustNode = it["gust"]
                                                                 gustNode?.textContent?.stringIfNotBlank()?.let {
                                                                     Speed(amount = it.toDouble(),
                                                                           unit = parseUnit(gustNode.attributeMap()["units"],
                                                                                            Speed.Units.values()))
                                                                 }
                                                             },

                                                             direction = run {
                                                                 val bearingNode = it["bearing"]
                                                                 bearingNode?.textContent?.stringIfNotBlank()?.let {
                                                                     Direction(amount = it.toDouble(),
                                                                               unit = parseUnit(bearingNode.attributeMap()["units"],
                                                                                                Direction.Units.values()))
                                                                 }
                                                             }
                                                         )
                                                 })
                            },

                            precipitation = it["precipitation"]?.let {
                                PrecipitationForecast(textSummary = it["textSummary"]?.textContent?.let { TextSummary() },
                                                      precipitationType = it["accumulation"]?.get("name")?.textContent?.let { PrecipitationType.get(it) },
                                                      accumulationSubstance = it["accumulation"]?.get("name")?.textContent?.let { AccumulationSubstance.get(it) },
                                                      accumulationAmount = it["accumulation"]?.get("amount")?.let {
                                                          AccumulationAmount(amount = it.textContent?.toDoubleOrNull() ?: 0.0,
                                                                             unit = parseUnit(it.attributeMap()["units"],
                                                                                              AccumulationAmount.Units.values()))
                                                      })
                            },

                            windChill = it["windChill"]?.let {
                                WindChillProperty(textSummary = it["textSummary"]?.textContent?.let { TextSummary(it) },
                                                  measure = it["calculated"]?.let {
                                                      WindChill(amount = it.textContent?.toDoubleOrNull() ?: 0.0,
                                                                unit = parseUnit(it.attributeMap()["units"],
                                                                                 WindChill.Units.values()))
                                                  })
                            },

                            visibility = it["visibility"]?.get("otherVisib")?.let {
                                Visibility(textSummary = it["textSummary"]?.textContent?.let { TextSummary(it) },
                                           cause = VisibilityCause.get(it.attributeMap()["cause"]))
                            },

                            uvIndexProperty = it["uv"]?.let {
                                UVIndexProperty(textSummary = it["textSummary"]?.textContent?.let { TextSummary(it) },
                                                uvIndex = UVIndex(amount = it["index"]?.textContent?.toDoubleOrNull() ?: 0.0))
                            },

                            humidity = it["humidity"]?.let {
                                Humidity(measure = Percentage(amount = it.textContent?.toDoubleOrNull() ?: 0.0,
                                                              unit = parseUnit(it.attributeMap()["units"],
                                                                               Percentage.Units.values())))
                            },

                            comfort = it["comfort"]?.get("textSummary")?.textContent?.let { TextSummary(it) },

                            frost = it["frost"]?.get("textSummary")?.textContent?.let { TextSummary(it) })

                } ?: emptyList<ForecastProperties>(),

                time = forecastGroup?.get("dateTime", mapOf(Pair("zone", "UTC")))
                        ?.get("timeStamp")?.textContent?.let { citypageProvider.parseTimestamp(it) },

                regionalNormals = forecastGroup?.get("regionalNormals")?.let { parseNodeOfTemperatures(it) })
    }

    private fun parseNodeOfTemperatures(e: Element) =
            TemperaturesWithSummary(textSummary = e["textSummary"]?.textContent?.let { TextSummary(it) },

                                    temperatures = e.getAll("temperature").map { node ->
                                        e.textContent?.stringIfNotBlank()?.let {
                                            TemperatureProperty(
                                                    temperature = Temperature(node.textContent.toDouble(),
                                                                              parseUnit(node.attributeMap()["units"],
                                                                                        Temperature.Units.values())),

                                                    type = node.attributeMap(true)["class"]?.let { TemperatureProperty.Type.get(it) })
                                        }
                                    }.filterNotNull())

    override fun writeToParcel(dest: Parcel, flags: Int) {
        CitypageWeatherProvider.packageSites(dest, flags)
    }

    companion object {
        @JvmField @Suppress("unused")
        val CREATOR: Parcelable.Creator<DailyCitypage> = object : Parcelable.Creator<DailyCitypage> {

            override fun createFromParcel(source: Parcel) = DailyCitypage(source)

            override fun newArray(size: Int) = arrayOfNulls<DailyCitypage?>(size)
        }
    }
}
