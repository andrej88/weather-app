package com.andrej.weatherapp.providers.CanadaWeatherOffice

import android.os.Parcel
import android.os.Parcelable
import com.andrej.weatherapp.properties.*
import com.andrej.weatherapp.properties.PropertyDataSets.CurrentWeatherProperties
import com.andrej.weatherapp.properties.ScalarQuantities.*
import com.andrej.weatherapp.providers.CurrentWeatherProvider
import com.andrej.weatherapp.util.attributeMap
import com.andrej.weatherapp.util.get
import com.andrej.weatherapp.util.parseUnit
import com.andrej.weatherapp.util.stringIfNotBlank

class CurrentCitypage(parcel: Parcel? = null) : CurrentWeatherProvider {

    private var citypageProvider = CitypageWeatherProvider()

    init {
        parcel?.let { CitypageWeatherProvider.unParcelSites(parcel) }
    }

    override fun acquire(location: LatLong): CurrentWeatherProperties {

        val root = citypageProvider.downloadXmlAndFindDocumentRoot(location)

        val ccNode = root["currentConditions"] ?: throw Exception("No current conditions found!")

        val temperatureNode = ccNode["temperature"]
        val dewpointNode = ccNode["dewpoint"]
        val pressureNode = ccNode["pressure"]
        val visibilityNode = ccNode["visibility"]
        val humidityNode = ccNode["relativeHumidity"]

        return CurrentWeatherProperties(
                location = ccNode["station"]?.let {
                    val lat = it.attributeMap()["lat"]
                    val long = it.attributeMap(true)["long"]
                    lat?.let { long?.let { LatLong(lat, long) } }
                },

                time = ccNode.get("dateTime", mapOf(Pair("zone", "UTC")))
                        ?.get("timeStamp")?.textContent?.let { citypageProvider.parseTimestamp(it) },

                textSummary = ccNode["condition"]?.textContent?.stringIfNotBlank()?.let {
                    TextSummary(it)
                },

                temperature = temperatureNode?.textContent?.stringIfNotBlank()?.let {
                    TemperatureProperty(
                            temperature = Temperature(amount = it.toDouble(),
                                                      unit = parseUnit(temperatureNode.attributeMap()["units"],
                                                                       Temperature.Units.values())),

                            type = TemperatureProperty.Type.values().find {
                                temperatureNode.attributeMap()["class"] in it.typeName
                            })
                },

                dewPoint = dewpointNode?.textContent?.stringIfNotBlank()?.let {
                    DewPoint(measure = Temperature(amount = it.toDouble(),
                                                   unit = parseUnit(dewpointNode.attributeMap()["units"],
                                                                    Temperature.Units.values())))
                },

                windChill = ccNode["windChill"]?.textContent?.stringIfNotBlank()?.let {
                    WindChillProperty(measure = WindChill(amount = it.toDouble(),
                                                          unit = WindChill.Units.CELSIUS))
                },

                canadianHumidex = ccNode["humidex"]?.textContent?.stringIfNotBlank()?.let {
                    CanadianHumidexProperty(measure = CanadianHumidex(amount = it.toDouble()))
                },

                pressure = pressureNode?.textContent?.stringIfNotBlank()?.let {
                    PressureProperty(measure = Pressure(amount = it.toDouble(),
                                                        unit = parseUnit(pressureNode.attributeMap()["units"],
                                                                         Pressure.Units.values())))
                },

                visibility = visibilityNode?.textContent?.stringIfNotBlank()?.let {
                    Visibility(measure = Distance(amount = it.toDouble(),
                                                  unit = parseUnit(visibilityNode.attributeMap()["units"],
                                                                   Distance.Units.values())))
                },

                humidity = humidityNode?.textContent?.stringIfNotBlank()?.let {
                    Humidity(measure = Percentage(amount = it.toDouble(),
                                                  unit = parseUnit(humidityNode.attributeMap()["units"],
                                                                   Percentage.Units.values())))
                },

                wind = ccNode["wind"]?.let {
                    Wind(
                            speed = run {
                                val speedNode = it["speed"]
                                speedNode?.textContent?.stringIfNotBlank()?.let {
                                    Speed(amount = it.toDouble(),
                                          unit = parseUnit(speedNode.attributeMap()["units"],
                                                           Speed.Units.values()))
                                }
                            },

                            gust = run {
                                val gustNode = it["gust"]
                                gustNode?.textContent?.stringIfNotBlank()?.let {
                                    Speed(amount = it.toDouble(),
                                          unit = parseUnit(gustNode.attributeMap()["units"],
                                                           Speed.Units.values()))
                                }
                            },

                            direction = run {
                                val bearingNode = it["bearing"]
                                bearingNode?.textContent?.stringIfNotBlank()?.let {
                                    Direction(amount = it.toDouble(),
                                              unit = parseUnit(bearingNode.attributeMap()["units"],
                                                               Direction.Units.values()))
                                }
                            }
                        )
                })

    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        CitypageWeatherProvider.packageSites(dest, flags)
    }

    companion object {
        @JvmField @Suppress("unused")
        val CREATOR: Parcelable.Creator<CurrentCitypage> = object : Parcelable.Creator<CurrentCitypage> {

            override fun createFromParcel(source: Parcel) = CurrentCitypage(source)

            override fun newArray(size: Int) = arrayOfNulls<CurrentCitypage?>(size)
        }
    }
}
