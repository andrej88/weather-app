package com.andrej.weatherapp.providers

import com.andrej.weatherapp.properties.PropertyDataSets.ForecastGroup

interface ForecastProvider : DataProvider<ForecastGroup>