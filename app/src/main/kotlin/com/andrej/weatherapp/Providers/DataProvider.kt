package com.andrej.weatherapp.providers

import android.os.Parcelable
import com.andrej.weatherapp.properties.LatLong

interface DataProvider<out T : Data> : Parcelable {

    fun acquire(location: LatLong): T

    override fun describeContents() = 0
}