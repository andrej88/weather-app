package com.andrej.weatherapp.providers

import com.andrej.weatherapp.properties.PropertyDataSets.NowcastingGroup

interface NowcastingProvider : DataProvider<NowcastingGroup>