package com.andrej.weatherapp.providers.CanadaWeatherOffice

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.util.addIfMissing
import com.andrej.weatherapp.util.addMissing
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.w3c.dom.Element
import java.net.URL
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import javax.xml.parsers.DocumentBuilderFactory

class CitypageWeatherProvider {

    private fun refreshSites() {

        val format = CSVFormat.newFormat(',').withHeader("Codes",
                                                         "English Names", "Province Codes",
                                                         "Latitude", "Longitude")

        val parser = CSVParser.parse(URL("http://dd.weatheroffice.gc.ca/citypage_weather/docs/site_list_en.csv"),
                                     Charset.forName("UTF-8"), format)

        val newSites = mutableListOf<Site>()

        for (record in parser) {

            if (record.first() == "Site Names" || record.first() == "Codes") continue

            if (record["Latitude"].isNullOrBlank() || record["Longitude"].isNullOrBlank()) {
                Log.i("CanadaCurrent", "Skipping coordinate-less site record ${record["Codes"]}")
                continue
            }
            newSites.addIfMissing(Site(record["Codes"],
                                       record["English Names"],
                                       record["Province Codes"],
                                       LatLong(record["Latitude"], record["Longitude"])))
            sites.clear()
            sites.addAll(newSites)
            sitesLastUpdatedTime = System.currentTimeMillis()
        }
    }

    fun downloadXmlAndFindDocumentRoot(location: LatLong): Element {
        if (sites.isEmpty() ||
            System.currentTimeMillis() - sitesLastUpdatedTime >= howOftenShouldSitesUpdate) {
            if (areSitesBeingUpdated.compareAndSet(false, true)) {
                refreshSites()
                areSitesBeingUpdated.set(false)
            }
            else {
                while (areSitesBeingUpdated.get()) {
                }
            }
        }

        val closestSite = sites.minBy { (it.location distanceTo location) } ?: throw Exception("No weather sites available!")

        if ((closestSite.location distanceTo location) > minDistance) {
            throw Exception("No suitable location found. Current location: $location, closest location: ${closestSite.location}")
        }

        val factory = DocumentBuilderFactory.newInstance()
        val builder = factory.newDocumentBuilder()
        val document = builder.parse("http://dd.weatheroffice.gc.ca/citypage_weather/xml/${closestSite.province}/${closestSite.code}_e.xml")

        return document.documentElement
    }

    fun parseTimestamp(timestamp: String): Calendar {
        val instance = Calendar.getInstance()
        val dateFormatter = SimpleDateFormat("yyyyMMddHHmmsszzz", Locale.US)
        instance.time = dateFormatter.parse("${timestamp}UTC")
        return instance
    }

    companion object {
        /**
         * Multiple fragments (thus multiple threads) may be trying to access the list of sites at the same time.
         * If one thread is already refreshing the site list, the other should wait for it to finish.
         */
        private val areSitesBeingUpdated = AtomicBoolean(false)

        /**
         * We don't want to parcel/un-parcel the site list multiple times - once will do.
         * The Provider that parcels the site list in its own parcel should remember that,
         * and everyone else should wait for the site list to be un-parceled before trying
         * to do anything with it.
         */
        val areSitesParceled = AtomicBoolean(false)

        private var sitesLastUpdatedTime = 0L
        private const val howOftenShouldSitesUpdate = 86400000L

        /**
         * List of all available locations
         */
        private val sites = mutableListOf<Site>()

        /**
         * If there is no site closer than this distance (in meters)
         */
        private const val minDistance = 50.0e3

        fun packageSites(dest: Parcel, flags: Int) {
            if (areSitesParceled.compareAndSet(false, true)) {

                // Lets us know that we have saved the list of sites in *this* Provider's parcel.
                // Yes, this one. It's a big responsibility, and *this* Provider got chosen! Isn't that exciting?!
                // Ints instead of Booleans? What is this, C?
                dest.writeInt(1)

                // Ok now save the actual list of sites and its size.
                dest.writeInt(sites.size)
                dest.writeTypedArray(sites.toTypedArray(), flags)
            }
            else {
                dest.writeInt(0)
            }
        }

        fun unParcelSites(parcel: Parcel) {
            if (parcel.readInt() != 0 && areSitesParceled.get()) {
                val sitesArray = arrayOfNulls<Site>(parcel.readInt())
                parcel.readTypedArray(sitesArray, Site.CREATOR)
                sites.addMissing(sitesArray.filterNotNull().toList())
                areSitesParceled.set(false)
            }
        }
    }

    data class Site(val code: String, val name: String,
                    val province: String, val location: LatLong) : Parcelable {

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeString(code)
            dest.writeString(name)
            dest.writeString(province)
            dest.writeParcelable(location, flags)
        }

        override fun describeContents() = 0

        companion object {
            @JvmField
            val CREATOR = object : Parcelable.Creator<Site> {

                override fun createFromParcel(source: Parcel) =
                        Site(source.readString(), source.readString(),
                             source.readString(), source.readParcelable(LatLong::class.java.classLoader))

                override fun newArray(size: Int): Array<out Site?> = arrayOfNulls(size)
            }
        }
    }
}