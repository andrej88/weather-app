package com.andrej.weatherapp.providers

import com.andrej.weatherapp.properties.PropertyDataSets.CurrentWeatherProperties

/**
 * Classes that implement this interface are expected to be able to provide a [CurrentWeatherProperties]
 * object, presumably by obtaining the relevant data from the internet.
 */
interface CurrentWeatherProvider : DataProvider<CurrentWeatherProperties>