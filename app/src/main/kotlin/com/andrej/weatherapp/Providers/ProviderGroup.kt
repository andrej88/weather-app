package com.andrej.weatherapp.providers

import com.andrej.weatherapp.providers.CanadaWeatherOffice.CurrentCitypage
import com.andrej.weatherapp.providers.CanadaWeatherOffice.DailyCitypage
import com.andrej.weatherapp.providers.CanadaWeatherOffice.HourlyNowcasting

data class ProviderGroup(val currentWeatherProvider: CurrentWeatherProvider? = null,
                         val dailyForecastProvider: ForecastProvider? = null,
                         val nowcastingProvider: NowcastingProvider? = null) {

    companion object {
        // Some pre-made providers
        val canadaProvider by lazy { ProviderGroup(currentWeatherProvider = CurrentCitypage(),
                                                   dailyForecastProvider = DailyCitypage(),
                                                   nowcastingProvider = HourlyNowcasting()) }
    }
}