package com.andrej.weatherapp.properties

import com.andrej.weatherapp.util.says

data class RelativeDay(val day: DayNames) {

    constructor(day: String) : this(DayNames.values().find { day says it.dayName }
                                    ?: DayNames.UNKNOWN)

    enum class DayNames(val dayName: String) {
        TODAY("today"),
        TONIGHT("tonight"),
        MONDAY("monday"),
        MONDAY_NIGHT("monday night"),
        TUESDAY("tuesday"),
        TUESDAY_NIGHT("tuesday night"),
        WEDNESDAY("wednesday"),
        WEDNESDAY_NIGHT("wednesday night"),
        THURSDAY("thursday"),
        THURSDAY_NIGHT("thursday night"),
        FRIDAY("friday"),
        FRIDAY_NIGHT("friday night"),
        SATURDAY("saturday"),
        SATURDAY_NIGHT("saturday night"),
        SUNDAY("sunday"),
        SUNDAY_NIGHT("sunday night"),
        UNKNOWN("Unknown day"),
    }
}