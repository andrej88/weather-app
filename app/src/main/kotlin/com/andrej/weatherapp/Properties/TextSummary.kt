package com.andrej.weatherapp.properties

data class TextSummary(val summaries: List<String> = emptyList())
    : List<String> by summaries {

    val shortest
        get() = summaries.minBy { it.length }

    val regular
        get() = summaries.sortedBy { it.length }[(summaries.size.takeIf { it % 2 == 0 } ?: (summaries.size + 1)) / 2]

    val longest
        get() = summaries.maxBy { it.length }

    constructor(summary: String) : this(listOf(summary))
}