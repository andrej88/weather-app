package com.andrej.weatherapp.properties.Qualities

enum class RiskLevel {
    NONE,
    RISK,
    CERTAIN
}