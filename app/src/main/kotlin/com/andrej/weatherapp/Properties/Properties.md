This package contains properties that might be provided by a weather service.

Classes for storing and easily making use of temperatures, wind velocities, precipitation periods, etc. can all be found here.

The properties and their implementations are meant to be service-agnostic and should minimize the amount of formatting and parsing code needed within provider-specific classes.

The PropertyDataSets package contains data classes that provide multiple properties that one might expect from a given service type.

The ScalarQuantities package contains measurable one-dimensional quantities, such as Temperature and Pressure. These classes allow easy conversion between units and string formatting.

These classes will also take care of unit conversions.