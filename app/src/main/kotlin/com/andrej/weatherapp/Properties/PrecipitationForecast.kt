package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.Qualities.AccumulationSubstance
import com.andrej.weatherapp.properties.Qualities.PrecipitationType
import com.andrej.weatherapp.properties.ScalarQuantities.AccumulationAmount
import java.util.*


data class PrecipitationForecast(val textSummary: TextSummary? = null,
                                 val precipitationType: PrecipitationType? = null,
                                 val accumulationSubstance: AccumulationSubstance? = null,
                                 val accumulationAmount: AccumulationAmount? = null,
                                 val start: Calendar? = null,
                                 val end: Calendar? = null)