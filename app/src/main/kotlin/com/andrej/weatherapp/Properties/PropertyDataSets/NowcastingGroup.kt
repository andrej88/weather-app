package com.andrej.weatherapp.properties.PropertyDataSets

import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.providers.Data
import java.util.*

data class NowcastingGroup(val nowcasts: List<NowcastingProperties> = emptyList(),
                           val location: LatLong? = null,
                           val time: Calendar? = null) : Data, Collection<NowcastingProperties> by nowcasts