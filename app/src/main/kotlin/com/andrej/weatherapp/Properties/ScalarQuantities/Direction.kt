package com.andrej.weatherapp.properties.ScalarQuantities

/**
 * Base unit: Degrees
 */
class Direction(amount: Double, unit: Units)
    : Measurable<Direction.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        DEGREES({ it }, { it }, "°", setOf("degrees", "degree", "°"));

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }

    val asCardinalDirection: String
        get() = when (amount % 360.0) {
            in 0.0..11.25 -> "N"
            in 11.25..33.75 -> "NNE"
            in 33.75..56.25 -> "NE"
            in 56.25..78.75 -> "ENE"
            in 78.75..101.25 -> "E"
            in 101.25..123.75 -> "ESE"
            in 123.75..146.25 -> "SE"
            in 146.25..168.75 -> "SSE"
            in 168.75..191.25 -> "S"
            in 191.25..213.75 -> "SSW"
            in 213.75..236.25 -> "SW"
            in 236.25..258.75 -> "WSW"
            in 258.75..281.25 -> "W"
            in 281.25..303.75 -> "WNW"
            in 303.75..326.25 -> "NW"
            in 326.25..348.75 -> "NNW"
            in 348.75..360.0 -> "N"
            else -> "???"
        }
}