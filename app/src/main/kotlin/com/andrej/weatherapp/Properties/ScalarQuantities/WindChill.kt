package com.andrej.weatherapp.properties.ScalarQuantities

class WindChill(amount: Double, unit: Units)
    : Measurable<WindChill.Units>(amount, unit) {

    /**
     * WindChill apparently doesn't have "units", it's just an index of some kind.
     * It is calculated using the following formula:
     *
     * `T_wc = 13.12 + (0.6215 * T_a) - (11.37 * V^0.16) + (0.3965 * T_a * V^0.16)`
     *
     * `T_wc`: the wind chill index.
     *
     * `T_a`: Air temperature in degrees Celsius
     *
     * `V`: Wind speed at 10 meters above the ground in km/h.
     *
     * But it looks like it's still a temperature...?
     */
    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        CELSIUS({ it }, { it }, " °C", setOf("C", "°C")),
        FAHRENHEIT({ (it - 32) / 1.8 }, { (it * 1.8) + 32 }, "°F", setOf("F", "°F")),
        KELVIN({ it + 273.15 }, { it - 273.15 }, " K", "K");

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }
}