package com.andrej.weatherapp.properties.ScalarQuantities

/**
 * Base unit: Percent
 */
class Percentage(amount: Double, unit: Units)
    : Measurable<Percentage.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        PERCENT({ it }, { it }, "%", "%"),
        REAL({ it * 100f }, { it / 100f }, ""),
        TENTHS({ it * 10 }, { Math.round(it / 10.0).toDouble() }, "/10"),
        EIGHTHS({ Math.round(it * (100.0 / 8.0)).toDouble() }, { Math.round((8.0 * it) / 100.0).toDouble() }, "/8");

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }

    companion object {
        val SOME_UNKNOWN_AMOUNT = Double.MIN_VALUE
    }
}