package com.andrej.weatherapp.properties.Qualities

enum class Tendency(override val typeNameForPrinting: String,
                    override val typeNames: Set<String>) : Quality {

    INCREASING("increasing", "increasing"),
    DEVELOPING("developing", "developing"),
    RISING("rising", "rising"),
    DISSIPATING("dissipating", "dissipating"),
    DECREASING("decreasing", "decreasing"),
    FALLING("rising", "rising"),
    CLEARING("clearing", "clearing"),
    STEADY("", emptySet());

    constructor(typeNameForPrinting: String, typeName: String) :
            this(typeNameForPrinting, setOf(typeName))

    companion object {
        fun get(name: String?) = name?.let { values().find { name.toLowerCase() in it.typeNames } }
    }
}