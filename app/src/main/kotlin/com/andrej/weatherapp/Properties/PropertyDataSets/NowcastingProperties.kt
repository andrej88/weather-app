package com.andrej.weatherapp.properties.PropertyDataSets

import com.andrej.weatherapp.properties.*
import com.andrej.weatherapp.providers.Data
import java.util.*

data class NowcastingProperties(val time: Calendar? = null,
                                val cloudCover: Cloudiness? = null,
                                val cloudCeiling: CloudCeiling? = null,
                                val precipitationChances: PrecipitationChances? = null,
                                val chanceOfPrecipitation: ChanceOfPrecipitation? = null,
                                val accumulation: PrecipitationAccumulation? = null,
                                val temperatureProperty: TemperatureProperty? = null,
                                val dewPoint: DewPoint? = null,
                                val wind: Wind? = null,
                                val visibility: Visibility? = null) : Data