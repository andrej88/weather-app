package com.andrej.weatherapp.properties.ScalarQuantities

class Pressure(amount: Double, unit: Units)
    : Measurable<Pressure.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        PASCAL({ it }, { it }, " Pa", setOf("Pa", "pa")),
        KILOPASCAL({ it * 1000 }, { it / 1000 }, " kPa", setOf("kPa", "kpa")),
        ATMOSPHERE({ it * 101325 }, { it / 101325 }, " atm", "atm"),
        MILLIBAR({ it * 100 }, { it / 100}, " mbar", setOf("mb", "mbar")),
        BAR({ it * 100000}, { it / 100000 }, " bar", setOf("b", "bar")),
        IN_HG({ it * 3386.389 }, { it / 3386.389 }, " inHg", setOf("in", "in.", "inHg")); // The US apparently measures pressure with a ruler but the ruler expands and contracts depending on the ambient temperature so an approximation is the best solution.

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }
}