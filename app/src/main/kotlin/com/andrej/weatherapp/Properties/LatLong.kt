package com.andrej.weatherapp.properties

import android.os.Parcel
import android.os.Parcelable
import com.andrej.weatherapp.properties.ScalarQuantities.Angle
import com.andrej.weatherapp.util.sin2

data class LatLong(val latitude: Angle,
                   val longitude: Angle) : Parcelable {

    /**
     * Creates a LatLong from something like ("123.24 W", "22N")
     */
    constructor(latitudeDeg: String, longitudeDeg: String) : this(

            Angle(amount = Regex("""-?\d*(\.\d+)?""").find(latitudeDeg)?.value?.toDouble()
                                   ?.times(if (latitudeDeg.contains('s', true)) {
                                       -1
                                   }
                                           else {
                                       1
                                   }) ?: throw Exception("Invalid latitudeDeg: $latitudeDeg"),
                  unit = Angle.Units.DEGREES),

            Angle(amount = Regex("""-?\d*(\.\d+)?""").find(longitudeDeg)?.value?.toDouble()
                                   ?.times(if (longitudeDeg.contains('w', true) || latitudeDeg.contains('-')) {
                                       -1
                                   }
                                           else {
                                       1
                                   }) ?: throw Exception("Invalid longitudeDeg: $longitudeDeg"),
                  unit = Angle.Units.DEGREES))

    constructor(latitudeDeg: Double, longitudeDeg: Double) : this(
            latitude = Angle(amount = latitudeDeg, unit = Angle.Units.DEGREES),
            longitude = Angle(amount = longitudeDeg, unit = Angle.Units.DEGREES)
                                                                 )


    constructor(source: Parcel) : this(source.readDouble(), source.readDouble())

    /**
     * Haversine formula
     *
     * @return distance in meters between a and other
     */
    infix fun distanceTo(other: LatLong): Double {

        val φ_this = latitude.inUnit(Angle.Units.RADIANS)
        val φ_other = other.latitude.inUnit(Angle.Units.RADIANS)
        val λ_this = longitude.inUnit(Angle.Units.RADIANS)
        val λ_other = other.longitude.inUnit(Angle.Units.RADIANS)
        val Δφ = φ_this - φ_other
        val Δλ = λ_this - λ_other

        val l = sin2(Δφ / 2) + Math.cos(φ_this) * Math.cos(φ_other) * sin2(Δλ / 2)
        return radiusOfEarth * 2 * Math.atan2(Math.sqrt(l), Math.sqrt(1 - l))
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeDouble(latitude.inUnit(Angle.Units.RADIANS))
        dest.writeDouble(longitude.inUnit(Angle.Units.RADIANS))
    }

    override fun describeContents() = 0

    companion object {

        @JvmField @Suppress("unused")
        val CREATOR = object : Parcelable.Creator<LatLong> {
            override fun createFromParcel(source: Parcel) = LatLong(source)

            override fun newArray(size: Int): Array<out LatLong?> = arrayOfNulls(size)
        }

        private val radiusOfEarth: Double = 6471e3
    }
}