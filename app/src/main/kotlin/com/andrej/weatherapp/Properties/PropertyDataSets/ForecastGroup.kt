package com.andrej.weatherapp.properties.PropertyDataSets

import com.andrej.weatherapp.properties.TemperaturesWithSummary
import com.andrej.weatherapp.providers.Data
import java.util.*

data class ForecastGroup(val forecasts: List<ForecastProperties> = emptyList(),
                         val time: Calendar? = null,
                         val regionalNormals: TemperaturesWithSummary? = null)
    : Data, Collection<ForecastProperties> by forecasts