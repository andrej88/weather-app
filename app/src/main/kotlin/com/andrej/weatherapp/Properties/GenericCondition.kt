package com.andrej.weatherapp.properties

import java.util.*

data class GenericCondition(val textSummary: TextSummary? = null,
                            val time: Calendar? = null,
                            val cloudiness: Cloudiness? = null,
                            val precipitationChances: PrecipitationChances? = null,
                            val visibility: Visibility? = null) {

    /**
     * Merges two SkyConditions by using the [other]'s property if the receiver's respective property is null.
     */
    infix fun merge(other: GenericCondition) =
            GenericCondition(textSummary = textSummary?.takeIf(List<String>::isNotEmpty) ?: other.textSummary,
                             time = time ?: other.time,
                             cloudiness = cloudiness ?: other.cloudiness,
                             precipitationChances = if (precipitationChances != null && other.precipitationChances != null) {
                                 precipitationChances merge other.precipitationChances
                             }
                             else {
                                 precipitationChances ?: other.precipitationChances
                             },
                             visibility = visibility ?: other.visibility)

    companion object {
        fun parseFromString(condition: String, time: Calendar?): GenericCondition {

            fun constructFromSingleString(condition: String) =
                    GenericCondition(cloudiness = Cloudiness.parseFromString(condition),
                                     precipitationChances = PrecipitationChances.parseFromString(condition),
                                     visibility = Visibility.parseFromString(condition))

            return condition
                    .split('.')
                    .map(::constructFromSingleString)
                    .fold(GenericCondition(textSummary = TextSummary(condition),
                                           time = time)) { acc, e -> acc merge e }
        }
    }
}