package com.andrej.weatherapp.properties.ScalarQuantities

/**
 *
 * Base unit: centimeters
 *
 * The Canada Weather Office usage of [amount] allows it to be either a String ("Trace") or a numeric amount.
 * Here, the "Trace" case is just Double.MIN_VALUE since things like Unions don't exist in Kotlin.
 */
class AccumulationAmount(amount: Double, unit: Units)
    : Measurable<AccumulationAmount.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {
        CENTIMETER({ it }, { it }, " cm", "cm"),
        MILLIMETER({ it / 10.0 }, { it * 10.0 }, " mm", "mm"),

        INCH({ it * 2.54 }, { it * 2.54 }, " in", setOf("in", "in.", "″", "\""));

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }

    companion object {
        val TRACE = Double.MIN_VALUE
    }
}