package com.andrej.weatherapp.properties.ScalarQuantities

abstract class Measurable<in T : Measurable.Units> protected constructor(amount: Double, unit: T) {

    protected val amount: Double = unit.toBase(amount)

    infix fun inUnit(unit: T) = unit.fromBase(amount)

    fun format(unit: T, formatString: String, printUnits: Boolean) =
            "${formatString.format(inUnit(unit))}${unit.symbolForPrinting.takeIf { printUnits } ?: ""}"

    override fun toString() = amount.toString()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Measurable<*>

        if (amount != other.amount) return false

        return true
    }

    override fun hashCode(): Int {
        return amount.hashCode()
    }

    interface Units {
        val toBase: (Double) -> Double
        val fromBase: (Double) -> Double
        val symbolForPrinting: String
        val symbols: Set<String>?
    }
}

