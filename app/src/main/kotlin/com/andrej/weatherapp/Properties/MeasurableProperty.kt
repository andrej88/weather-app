package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.ScalarQuantities.Measurable

open class MeasurableProperty<out T : Measurable<*>>(open val textSummary: TextSummary? = null,
                                                     open val measure: T? = null)