package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.Qualities.AccumulationSubstance
import com.andrej.weatherapp.properties.ScalarQuantities.AccumulationAmount

data class PrecipitationAccumulation(override val textSummary: TextSummary? = null,
                                     override val measure: AccumulationAmount? = null,
                                     val accumulationSubstance: AccumulationSubstance? = null)
    : MeasurableProperty<AccumulationAmount>(textSummary, measure)