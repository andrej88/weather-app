package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.Qualities.Tendency
import com.andrej.weatherapp.properties.ScalarQuantities.Percentage

data class Cloudiness(override val textSummary: TextSummary? = null,
                      override val measure: Percentage? = null,
                      val tendency: Tendency? = null)
    : MeasurableProperty<Percentage>(textSummary, measure) {

    companion object {

        private val stringToCoverMap = mapOf(
                // Clear
                "sunny" to 0.0,
                "clear" to 0.0,

                "mainly sunny" to 1.0,
                "mostly sunny" to 1.0,
                "mainly clear" to 1.0,
                "mostly clear" to 1.0,
                "a few clouds" to 1.0,

                // 50/50
                "partly cloudy" to 3.0,
                "sunny with cloudy periods" to 3.0,
                "a mix of sun and cloud" to 4.0,

                // Mostly cloudy
                "partly sunny" to 5.0,
                "partly clear" to 5.0,
                "mainly cloudy" to 7.0,
                "mostly cloudy" to 7.0,
                "cloudy with sunny periods" to 7.0,

                // Cloudy
                "cloudy" to 8.0,
                "overcast" to 8.0)

        private val coverPattern =
                """(${stringToCoverMap.keys
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        private val tendencyPattern =
                """(${Tendency.values()
                        .flatMap(Tendency::typeNames)
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        fun parseFromString(condition: String?) =
                condition?.takeIf(String::isNotBlank)?.let {
                    val clean = condition.trim(' ', '.').toLowerCase()
                    val cloudiness = Regex("""(($coverPattern)(\W|$))|$tendencyPattern""").find(clean)?.value
                    cloudiness?.let {
                        val cover = Regex(coverPattern).find(cloudiness)?.value
                        val tendency = Regex(tendencyPattern).find(cloudiness)?.value
                        stringToCoverMap[cover?.trim('.', ' ')?.toLowerCase()].let {
                            Cloudiness(textSummary = TextSummary(cloudiness.capitalize()),
                                       measure = Percentage(it ?: 4.0, Percentage.Units.EIGHTHS),
                                       tendency = Tendency.get(tendency))
                        }
                    }
                }
    }

}