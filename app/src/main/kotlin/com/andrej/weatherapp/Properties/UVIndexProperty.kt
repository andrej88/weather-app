package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.ScalarQuantities.UVIndex

data class UVIndexProperty(val textSummary: TextSummary? = null,
                      val uvIndex: UVIndex? = null) {

    val category: Category?

    init {
        if (uvIndex == null) {
            category = null
        }
        else {
            val indexInUnit = uvIndex.inUnit(UVIndex.Dimensionless)
            when {
                indexInUnit.isNaN() -> category = null
                indexInUnit < 0 -> category = null
                indexInUnit in 0.0..3.0 -> category = Category.LOW
                indexInUnit in 3.0..6.0 -> category = Category.MODERATE
                indexInUnit in 6.0..8.0 -> category = Category.HIGH
                indexInUnit in 8.0..11.0 -> category = Category.VERY_HIGH
                else -> category = Category.EXTREME
            }
        }
    }

    enum class Category(val categoryName: String) {
        LOW("low"),
        MODERATE("moderate"),
        HIGH("high"),
        VERY_HIGH("very high"),
        EXTREME("extreme")
    }
}