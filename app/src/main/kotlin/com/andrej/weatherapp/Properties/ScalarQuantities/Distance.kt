package com.andrej.weatherapp.properties.ScalarQuantities

/**
 * Base unit: Kilometer
 */
class Distance(amount: Double, unit: Units)
    : Measurable<Distance.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        KILOMETER({ it }, { it }, " km", "km"),
        MILE({ it / 1.60934 }, { it * 1.60934 }, "mi", setOf("mi", "mile", "miles")),
        METER({ it / 1000 }, { it * 1000 }, " m", "m"),
        FEET({it / 3280.840 }, {it * 3280.840}, " ft", "ft")
        ;

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }
}