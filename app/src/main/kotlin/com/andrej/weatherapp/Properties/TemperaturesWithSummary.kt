package com.andrej.weatherapp.properties

/**
 * Multiple temperature properties with a text summary explaining their meaning.
 *
 * Example:
 *
 * [temperatures] = [ TemperatureProperty(4, CELSIUS, type = LOW)  TemperatureProperty(10, CELSIUS, type = HIGH) ]
 *
 * [textSummary].[regular][TextSummary.regular] = "Low 4 with temperature rising to a high of 10 by noon"
 */
data class TemperaturesWithSummary(val textSummary: TextSummary? = null,
                                   val temperatures: List<TemperatureProperty> = emptyList())