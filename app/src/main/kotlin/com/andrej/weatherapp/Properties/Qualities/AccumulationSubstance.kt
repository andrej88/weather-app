package com.andrej.weatherapp.properties.Qualities

/**
 * This is distinct from [PrecipitationType], since this looks at what is on the ground.
 */
enum class AccumulationSubstance(override val typeNameForPrinting: String,
                                 override val typeNames: Set<String>)
    : Quality {

    RAIN("rain", setOf("rain", "water")),
    SNOW("snow", setOf("snow", "sn")),
    ICE("ice", setOf("ice", "freezing")),
    ICE_PELLETS("ice pellets", setOf("ice pellets", "pellets"));

    constructor(typeNameForPrinting: String, typeName: String) :
            this(typeNameForPrinting, setOf(typeName))

    companion object {
        fun get(name: String?) = name?.let { values().find { name.toLowerCase() in it.typeNames } }
    }
}