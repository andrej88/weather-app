package com.andrej.weatherapp.properties.Qualities

enum class PrecipitationType(override val typeNameForPrinting: String,
                             override val typeNames: Set<String>)
    : Quality {

    RAIN("rain", setOf("rain", "water", "r")),
    RAIN_SHOWER("rain showers", setOf("rain showers", "rain shower", "showers", "shower", "rw")),
    SNOW("snow", setOf("snow", "s")),
    FLURRY("flurries", setOf("snow showers", "snow shower", "flurries", "flurry", "sw")),
    WET_FLURRY("wet flurries", setOf("wet flurries", "wet flurry")),
    DRIZZLE("drizzle", setOf("drizzle", "l")),
    FREEZING_RAIN("freezing rain", setOf("freezing rain", "zr")),
    FREEZING_DRIZZLE("freezing drizzle", setOf("freezing drizzle", "zl")),
    ICE_PELLET("ice pellets", setOf("ice pellet", "ice pellets", "pellets", "ip")),
    ICE_PELLET_SHOWER("ice pellet shower", setOf("ice pellet shower", "ipw")),
    ICE_CRYSTAL("ice crystals", "ice crystals"),
    SNOW_PELLET("snow pellets", "snow pellets"),
    SNOW_GRAIN("snow grains", "snow grains"),
    THUNDER("thunder", setOf("thunder", "t")),
    HAIL("hail", setOf("hail", "a")),
    PRECIPITATION("precipitation", "precipitation");

    constructor(typeNameForPrinting: String, typeName: String) :
            this(typeNameForPrinting, setOf(typeName))

    companion object {

        // Trust me, those two '-' are different. First one is a hyphen, second is hyphen-minus
        fun get(name: String?) = name?.let { values().find { name.trim('+', '-', '-').toLowerCase() in it.typeNames } }

    }
}