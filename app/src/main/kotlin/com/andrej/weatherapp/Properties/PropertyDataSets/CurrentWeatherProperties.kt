package com.andrej.weatherapp.properties.PropertyDataSets

import com.andrej.weatherapp.properties.*
import com.andrej.weatherapp.providers.Data
import java.util.*

/**
 * @param location Coordinates at which the measurements were done
 * @param time Time at which the data was acquired
 * @param textSummary "short" is a natural language description of the current conditions, e.g. "Sunny"
 * @param temperature Current temperature
 * @param dewPoint Temperature at which water would condense, given the current humidity.
 * @param windChill Wind chill factor
 * @param canadianHumidex Canadian humidex value
 * @param pressure Current pressure
 * @param visibility Distance of farthest visible ground-level objects in current conditions
 * @param humidity Amount of water dissolved in air as a percent of the total water that can be dissolved in the air at this temperature
 * @param wind Speed of the wind and direction it is blowing from
 * @param cloudiness Percentage of sky that is covered by clouds
 * @param airQuality Quality of air, presumably based on factors such as smog
 * @param pollen Pollen allergy risk level
 * @param uvIndex UV index
 */
data class CurrentWeatherProperties(val location: LatLong? = null,
                                    val time: Calendar? = null,
                                    val textSummary: TextSummary? = null,
                                    val temperature: TemperatureProperty? = null,
                                    val dewPoint: DewPoint? = null,
                                    val windChill: WindChillProperty? = null,
                                    val canadianHumidex: CanadianHumidexProperty? = null,
                                    val pressure: PressureProperty? = null,
                                    val visibility: Visibility? = null,
                                    val humidity: Humidity? = null,
                                    val wind: Wind? = null,
                                    val cloudiness: Cloudiness? = null,
                                    val airQuality: Int? = null,
                                    val pollen: Int? = null,
                                    val uvIndex: UVIndexProperty? = null
                                   ) : Data