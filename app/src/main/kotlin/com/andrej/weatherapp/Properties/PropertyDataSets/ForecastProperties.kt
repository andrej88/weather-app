package com.andrej.weatherapp.properties.PropertyDataSets

import com.andrej.weatherapp.properties.*
import com.andrej.weatherapp.properties.ScalarQuantities.AccumulationAmount

data class ForecastProperties(val period: RelativeDay? = null,
                              val textSummary: TextSummary? = null,
                              val chanceOfPrecipitation: ChanceOfPrecipitation? = null,
                              val temperatures: TemperaturesWithSummary? = null,
                              val winds: WindsWithSummary? = null,
                              val precipitation: PrecipitationForecast? = null,
                              val snowLevel: AccumulationAmount? = null,
                              val windChill: WindChillProperty? = null,
                              val visibility: Visibility? = null,
                              val uvIndexProperty: UVIndexProperty? = null,
                              val humidity: Humidity? = null,
                              val comfort: TextSummary? = null,
                              val frost: TextSummary? = null)