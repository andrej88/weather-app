package com.andrej.weatherapp.properties

/**
 * Type aliases for simple properties, i.e. those that only have a TextSummary and a Measure
 */

import com.andrej.weatherapp.properties.ScalarQuantities.*

typealias CanadianHumidexProperty = MeasurableProperty<CanadianHumidex>
typealias ChanceOfPrecipitation = MeasurableProperty<Percentage>
typealias CloudCeiling = MeasurableProperty<Distance>
typealias DewPoint = MeasurableProperty<Temperature>
typealias Humidity = MeasurableProperty<Percentage>
typealias WindChillProperty = MeasurableProperty<WindChill>