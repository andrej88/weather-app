package com.andrej.weatherapp.properties.ScalarQuantities

/**
 * Base unit: Celsius
 */
open class Temperature(amount: Double, unit: Units)
    : Measurable<Temperature.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        CELSIUS({ it }, { it }, " °C", setOf("C", "°C")),
        FAHRENHEIT({ (it - 32) / 1.8 }, { (it * 1.8) + 32 }, "°F", setOf("F", "°F")),
        KELVIN({ it + 273.15 }, { it - 273.15 }, " K", "K");

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }
}