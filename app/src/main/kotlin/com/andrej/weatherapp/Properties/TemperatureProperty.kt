package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.ScalarQuantities.Temperature

data class TemperatureProperty(val textSummary: TextSummary? = null,
                               val temperature: Temperature? = null,
                               val type: Type? = null) {

    enum class Type(val typeName: Set<String>) {
        HIGH("high"),
        LOW("low"),
        MAX("max"),
        MIN("min"),
        EXTREME_MAX("extremeMax"),
        EXTREME_MIN("extremeMin"),
        NORMAL_MAX("normalMax"),
        NORMAL_MIN("normalMin"),
        MEAN("mean"),
        NORMAL_MEAN("normalMean"),
        CURRENT("current"),
        DEW_POINT("dewpoint");

        constructor(typeName: String) : this(setOf(typeName))

        companion object {
            fun get(type: String) = values().find { type.toLowerCase() in it.typeName.map(String::toLowerCase) }
        }
    }
}