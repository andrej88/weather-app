package com.andrej.weatherapp.properties.ScalarQuantities

class UVIndex(amount: Double)
    : Measurable<UVIndex.Dimensionless>(amount, Dimensionless) {

    object Dimensionless : Measurable.Units {
        override val toBase: (Double) -> Double = { it }
        override val fromBase: (Double) -> Double = { it }
        override val symbolForPrinting = ""
        override val symbols: Set<String> = emptySet()
    }
}