package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.Qualities.PrecipitationType
import com.andrej.weatherapp.properties.Qualities.RiskLevel
import com.andrej.weatherapp.properties.ScalarQuantities.Percentage

data class PrecipitationChances(val textSummary: TextSummary? = null,
                                val totalChance: ChanceOfPrecipitation? = null,
                                val precipitations: Set<WeightedPrecipitation> = emptySet(),
                                val thunderRisk: RiskLevel = RiskLevel.NONE)
    : Set<PrecipitationChances.WeightedPrecipitation> by precipitations {

    data class WeightedPrecipitation(val type: PrecipitationType,
                                     val weight: Double? = null) {

        /**
         * Override equals and hashCode because we don't want the set of [WeightedPrecipitation] to have multiple
         * entries of the same precipitation type.
         */
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is WeightedPrecipitation) return false

            return type == other.type
        }

        override fun hashCode() = type.hashCode()
    }

    infix fun merge(other: PrecipitationChances) =
            PrecipitationChances(textSummary = textSummary?.takeIf(List<String>::isNotEmpty) ?: other.textSummary,
                                 totalChance = totalChance ?: other.totalChance,
                                 precipitations = mutableSetOf<WeightedPrecipitation>().apply {
                                     addAll(precipitations)
                                     addAll(other.precipitations)
                                 },
                                 thunderRisk = thunderRisk.takeIf { it != RiskLevel.NONE } ?: other.thunderRisk)

    companion object {

        private val chancePattern =
                """(${listOf("chance of", "risk of", "possible")
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        private val typesPattern =
                """(${PrecipitationType.values()
                        .flatMap(PrecipitationType::typeNames)
                        .filterNot { it.length == 1 || it.length == 2 } // The short ones are for nowcasting, where we can use a simple get() anyway
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        private val intensityPattern =
                """(${listOf("light", "heavy", "medium")
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        private val stormPattern =
                """(${listOf("thunderstorm", "thundershower")
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        fun parseFromString(chances: String?): PrecipitationChances? =
                chances?.takeIf(String::isNotBlank)?.let {
                    val clean = chances.trim().toLowerCase()
                    val pattern = Regex("""($chancePattern\s+)?(.*\s+)?($intensityPattern\s+)?(.*\s+)?($typesPattern(\s+(or|and)\s+$typesPattern)?)""")

                    val value = pattern.find(clean)?.value
                    value?.let {

                        val numericalChance = Regex("""((\d?\d)\s*(%|percent))\s+$chancePattern""").find(it)?.value?.let {
                            Regex("""(\d?\d)""").find(it)?.value?.toDouble()
                        } ?: Regex(chancePattern).find(it)?.value?.let { Percentage.SOME_UNKNOWN_AMOUNT }

                        val matches = Regex(typesPattern).findAll(clean).map(MatchResult::value)

                        val storm = Regex("""($chancePattern\s+(a?\s+))?$stormPattern""").find(clean)?.value
                        val isStormChance = storm?.let { Regex(chancePattern).find(storm)?.value != null } ?: false

                        PrecipitationChances(textSummary = TextSummary(chances),
                                             totalChance = ChanceOfPrecipitation(measure = Percentage(numericalChance ?: 100.0,
                                                                                                      Percentage.Units.PERCENT)),
                                             precipitations = matches.map { PrecipitationType.get(it) }
                                                     .filterNotNull()
                                                     .map { WeightedPrecipitation(it) }.toSet(),

                                             thunderRisk =
                                             if (isStormChance) {
                                                 RiskLevel.RISK
                                             }
                                             else if (storm != null) {
                                                 RiskLevel.CERTAIN
                                             }
                                             else {
                                                 RiskLevel.NONE
                                             })
                    }
                }
    }
}