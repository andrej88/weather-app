package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.ScalarQuantities.Direction
import com.andrej.weatherapp.properties.ScalarQuantities.Speed

/**
 * [direction] is where the wind is blowing *from*.
 */
data class Wind(val textSummary: TextSummary? = null,
                val speed: Speed? = null,
                val gust: Speed? = null,
                val direction: Direction? = null,
                val index: Int? = null,
                val rank: Rank? = null) {

    enum class Rank {
        MINOR, MAJOR
    }
}