package com.andrej.weatherapp.properties.Qualities

interface Quality {
    val typeNameForPrinting: String
    val typeNames: Set<String>
}