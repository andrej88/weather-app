package com.andrej.weatherapp.properties.Qualities

enum class VisibilityCause(override val typeNameForPrinting: String,
                           override val typeNames: Set<String>)
    : Quality {

    PRECIPITATION("precipitation", setOf("precipitation", "pp")),
    FOG("fog", setOf("fog", "fg")),
    FOG_BANKS("fog banks", setOf("fog banks", "bf")),
    FOG_PATCHES("fog patches", "fog patches"),
    SHALLOW_FOG("shallow fog", "shallow fog"),
    MIST("mist", setOf("mist", "br")),
    RISK_OF_FOG("risk of fog", setOf("risk of fog", "risk fog", "rf")),
    ICE_FOG("ice fog", setOf("ice fog", "if")),
    BLOWING_SNOW("blowing snow", setOf("blowing snow", "bs")),
    DRIFTING_SNOW("drifting snow", setOf("drifting snow", "ds")),
    HAZE("haze", setOf("haze", "hz")),
    SMOKE("smoke", setOf("smoke", "fu")),
    BLOWING_DUST("blowing dust", setOf("blowing dust", "bd"));

    constructor(typeNameForPrinting: String, typeName: String) :
            this(typeNameForPrinting, setOf(typeName))

    companion object {
        fun get(name: String?) = name?.let { values().find { name.toLowerCase() in it.typeNames } }
    }
}