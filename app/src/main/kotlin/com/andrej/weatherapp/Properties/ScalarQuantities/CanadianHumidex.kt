package com.andrej.weatherapp.properties.ScalarQuantities

class CanadianHumidex(amount: Double)
    : Measurable<CanadianHumidex.Dimensionless>(amount, Dimensionless) {

    /**
     * No units. From Wikipedia:
     *
     * The humidex is a dimensionless quantity based on the dew point.
     *
     * Range of humidex: Degree of comfort:
     *
     * 20 to 29: Little to no discomfort
     * 30 to 39: Some discomfort
     * 40 to 45: Great discomfort; avoid exertion
     * Above 45: Dangerous; heat stroke quite possible
     *
     */
    object Dimensionless : Measurable.Units {
        override val toBase: (Double) -> Double = { it }
        override val fromBase: (Double) -> Double = { it }
        override val symbolForPrinting = ""
        override val symbols: Set<String> = emptySet()
    }
}