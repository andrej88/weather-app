package com.andrej.weatherapp.properties.ScalarQuantities

/**
 * Base unit: km/h
 */
class Speed(amount: Double, unit: Units)
    : Measurable<Speed.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String> = emptySet()) : Measurable.Units {

        KMH({ it }, { it }, " km/h", setOf("km/h", "kph", "kmh")),
        MPH({ it * 1.60934 }, { it / 1.60934 }, " mph", setOf("MPH", "mph")),
        MPS({ it * 3.6 }, { it / 3.6 }, " m·s⁻¹", setOf("m/s", "ms⁻¹", "m·s⁻¹"));

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }
}