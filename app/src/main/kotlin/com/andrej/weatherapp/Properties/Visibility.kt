package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.Qualities.Tendency
import com.andrej.weatherapp.properties.Qualities.VisibilityCause
import com.andrej.weatherapp.properties.ScalarQuantities.Distance

data class Visibility(override val textSummary: TextSummary? = null,
                      override val measure: Distance? = null,
                      val cause: VisibilityCause? = null,
                      val tendency: Tendency? = null)
    : MeasurableProperty<Distance>(textSummary, measure) {

    companion object {

        private val visibilityPattern =
                """(${VisibilityCause.values()
                        .flatMap(VisibilityCause::typeNames)
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        private val tendencyPattern =
                """(${Tendency.values()
                        .flatMap(Tendency::typeNames)
                        .reduce { acc, e -> acc + "|" + e }
                        .trim('|')})"""

        fun parseFromString(visibility: String?) =
                visibility?.takeIf(String::isNotBlank)?.let {
                    val clean = visibility.trim().toLowerCase()
                    Regex("""$visibilityPattern\s+.*\s+($tendencyPattern)?""").find(clean)?.value?.let {
                        val cause = VisibilityCause.get(Regex(visibilityPattern).find(it)?.value)
                        val tendency = Tendency.get(Regex(tendencyPattern).find(it)?.value)
                        if (cause != null && tendency != null) {
                            Visibility(cause = cause, tendency = tendency)
                        }
                        else {
                            null
                        }
                    }
                }
    }

}