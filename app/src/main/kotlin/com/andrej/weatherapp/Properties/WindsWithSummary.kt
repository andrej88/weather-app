package com.andrej.weatherapp.properties

/**
 * Multiple winds with a text summary explaining their meaning.
 *
 * @see TemperaturesWithSummary
 */
data class WindsWithSummary(val textSummary: TextSummary? = null,
                            val winds: List<Wind> = emptyList())