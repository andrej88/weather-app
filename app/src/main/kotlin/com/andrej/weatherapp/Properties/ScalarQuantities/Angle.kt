package com.andrej.weatherapp.properties.ScalarQuantities

class Angle(amount: Double, unit: Units)
    : Measurable<Angle.Units>(amount, unit) {

    enum class Units(override val toBase: (Double) -> Double,
                     override val fromBase: (Double) -> Double,
                     override val symbolForPrinting: String,
                     override val symbols: Set<String>?) : Measurable.Units {

        DEGREES({ it }, { it }, "°", "°"),
        RADIANS(Math::toDegrees, Math::toRadians, "°", setOf("rad", "r"))
        ;

        constructor(toBase: (Double) -> Double,
                    fromBase: (Double) -> Double,
                    symbolForPrinting: String,
                    symbol: String) : this(toBase, fromBase, symbolForPrinting, setOf(symbol))
    }
}