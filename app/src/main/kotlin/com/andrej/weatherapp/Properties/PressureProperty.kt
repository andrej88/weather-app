package com.andrej.weatherapp.properties

import com.andrej.weatherapp.properties.Qualities.Tendency
import com.andrej.weatherapp.properties.ScalarQuantities.Pressure

data class PressureProperty(override val textSummary: TextSummary? = null,
                            override val measure: Pressure? = null,
                            val change: Pressure? = null,
                            val tendency: Tendency? = null)
    : MeasurableProperty<Pressure>(textSummary, measure)