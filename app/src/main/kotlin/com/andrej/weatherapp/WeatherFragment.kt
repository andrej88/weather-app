package com.andrej.weatherapp

import android.support.v4.app.Fragment

abstract class WeatherFragment : Fragment() {
    abstract fun refreshData(userTriggered: Boolean = false)
}