package com.andrej.weatherapp

import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.properties.ScalarQuantities.*
import com.andrej.weatherapp.providers.NowcastingProvider
import com.andrej.weatherapp.util.imageViewCompat
import com.andrej.weatherapp.util.lineChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachWithIndex
import java.text.SimpleDateFormat
import java.util.*

class HourlyForecastFragment() : ForecastFragmentBase() {

    lateinit override var location: LatLong

    lateinit var provider: NowcastingProvider
    private val providerKey: String = NowcastingProvider::class.java.canonicalName

    private var graphTempColor = 0
    lateinit private var graphTempTitle: String
    lateinit private var temperatureGraph: LineChart

    constructor(location: LatLong) : this() {
        this.location = location
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            provider = savedInstanceState.get(providerKey) as NowcastingProvider
            location = savedInstanceState.get(locationKey) as LatLong
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        @Suppress("DEPRECATION")
        graphTempColor = resources.getColor(R.color.graph_temp_high)

        graphTempTitle = resources.getString(R.string.temperature_hl_graph_high_title)

        refreshData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(providerKey, provider)
        outState.putParcelable(locationKey, location)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = ForecastFragmentUi().createView(AnkoContext.create(activity, this))

    override fun refreshData(userTriggered: Boolean) {

        showLoading()

        doAsync {
            val data = provider.acquire(location)
            uiThread {

                forecastList.removeAllViews()

                val temps = mutableListOf<Entry>()

                val now = Calendar.getInstance()
                with(forecastList) {
                    linearLayout {

                        verticalLayout {
                            createTableHeaderCell(R.drawable.quantity_time, this)
                            data.nowcasts.forEach { (time, _, _, _, _, _, _, _, _, _) ->
                                if (time?.after(now) ?: true) {

                                    linearLayout {

                                        lparams {
                                            topMargin = dip(8f)
                                            bottomMargin = dip(8f)
                                            leftMargin = dip(8f)
                                            rightMargin = dip(8f)
                                        }

                                        createCell(SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT).format(time?.time), this)
                                    }
                                }
                            }
                        }

                        verticalLayout {
                            createTableHeaderCell(R.drawable.quantity_temperature, this, Temperature.Units.CELSIUS)
                            data.nowcasts.forEachWithIndex { index, (time, _, _, _, _, _, temperatureProperty, _, _, _) ->
                                if (time?.after(now) ?: true) {

                                    linearLayout {

                                        lparams {
                                            topMargin = dip(8f)
                                            bottomMargin = dip(8f)
                                            leftMargin = dip(8f)
                                            rightMargin = dip(8f)
                                        }

                                        createCell(temperatureProperty?.temperature?.format(Temperature.Units.CELSIUS, "%.0f", false), this)
                                    }

                                    temperatureProperty?.temperature?.let {
                                        temps.add(Entry(index.toFloat(), (it inUnit Temperature.Units.CELSIUS).toFloat()))
                                    }
                                }
                            }
                        }

                        verticalLayout {
                            createTableHeaderCell(R.drawable.quantity_humidity, this, AccumulationAmount.Units.CENTIMETER)
                            data.nowcasts.forEach { (time, _, _, _, _, accumulation, _, _, _, _) ->
                                if (time?.after(now) ?: true) {

                                    linearLayout {

                                        lparams {
                                            topMargin = dip(8f)
                                            bottomMargin = dip(8f)
                                            leftMargin = dip(8f)
                                            rightMargin = dip(8f)
                                        }

                                        createCell(accumulation?.measure?.format(AccumulationAmount.Units.CENTIMETER, "%.1f", false), this)
                                    }
                                }
                            }
                        }

                        verticalLayout {
                            createTableHeaderCell(R.drawable.condition_rain, this, Percentage.Units.PERCENT)
                            data.nowcasts.forEach { (time, _, _, _, chanceOfPrecipitation, _, _, _, _, _) ->
                                if (time?.after(now) ?: true) {

                                    linearLayout {

                                        lparams {
                                            topMargin = dip(8f)
                                            bottomMargin = dip(8f)
                                            leftMargin = dip(8f)
                                            rightMargin = dip(8f)
                                        }

                                        createCell(chanceOfPrecipitation?.measure?.format(Percentage.Units.PERCENT, "%.0f", false), this)
                                    }
                                }
                            }
                        }

                        verticalLayout {
                            createTableHeaderCell(R.drawable.quantity_wind, this, Speed.Units.KMH)
                            data.nowcasts.forEach { (time, _, _, _, _, _, _, _, wind, _) ->
                                if (time?.after(now) ?: true) {

                                    linearLayout {

                                        lparams {
                                            topMargin = dip(8f)
                                            bottomMargin = dip(8f)
                                            leftMargin = dip(8f)
                                            rightMargin = dip(8f)
                                        }

                                        createCell(wind?.speed?.format(Speed.Units.KMH, "%.0f", false), this)
                                    }
                                }
                            }
                        }
                    }
                }


                val tempSet = LineDataSet(temps, graphTempTitle)
                with(tempSet) {
                    color = graphTempColor
                    setDrawCircleHole(false)
                    setCircleColor(graphTempColor)

                    @Suppress("DEPRECATION")
                    valueTextColor = resources.getColor(R.color.textLight)
                }


                val lineData = LineData(tempSet)
                temperatureGraph.data = lineData
                temperatureGraph.invalidate()

                hideLoading()

            }
        }

    }

    private fun createTableHeaderCell(imageResource: Int, parent: LinearLayout, unit: Measurable.Units? = null) {
        with(parent) {
            linearLayout {
                layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent).also {
                    gravity = Gravity.CENTER
                }

                imageViewCompat {
                    this.imageResource = imageResource
                    layoutParams = ViewGroup.LayoutParams(matchParent, dip(32f)).also {
                        gravity = Gravity.CENTER
//                    topMargin = dip(8f)
//                    bottomMargin = dip(8f)
//                    leftMargin = dip(8f)
//                    rightMargin = dip(8f)
                    }
                }

                unit?.let {
                    textView {
                        this.text = unit.symbolForPrinting
                        textSize = dip(12f).toFloat()
                        @Suppress("DEPRECATION")
                        textColor = resources.getColor(R.color.textLight)
                    }
                }
            }
        }
    }

    private fun createCell(text: String?, parent: LinearLayout) {

        with(parent) {
            textView {

                layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent).also {
                    gravity = Gravity.END
                }

                this.text = text ?: ""
                textSize = dip(16f).toFloat()
                @Suppress("DEPRECATION")
                textColor = resources.getColor(R.color.textLight)
                gravity = Gravity.END
            }
        }
    }


    override fun createForecastList(viewManager: ViewManager) =
            viewManager.verticalLayout {
                lparams(width = matchParent, height = wrapContent)
            }


    override fun createGraphs(viewManager: ViewManager) =
            viewManager.verticalLayout {

                temperatureGraph = lineChart {
                    setNoDataText(resources.getString(R.string.temperature_hl_graph_no_data))
                    setTouchEnabled(false)
                    axisLeft.setDrawZeroLine(true)
                }.lparams {
                    width = matchParent
                    height = dip(200f)
                    leftMargin = dip(8f)
                    rightMargin = dip(8f)
                }

                lparams(width = matchParent, height = wrapContent)
            }
}