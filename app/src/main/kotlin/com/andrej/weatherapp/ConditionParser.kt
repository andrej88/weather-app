package com.andrej.weatherapp

import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.util.Log
import com.andrej.weatherapp.properties.GenericCondition
import com.andrej.weatherapp.properties.PrecipitationChances
import com.andrej.weatherapp.properties.Qualities.PrecipitationType
import com.andrej.weatherapp.properties.Qualities.RiskLevel
import com.andrej.weatherapp.properties.Qualities.Tendency
import com.andrej.weatherapp.properties.Qualities.VisibilityCause
import com.andrej.weatherapp.properties.ScalarQuantities.Percentage
import com.andrej.weatherapp.util.ChancePrecipitationDrawable
import com.andrej.weatherapp.util.getVectorDrawable
import java.util.*

@Suppress("DEPRECATION")
class ConditionParser(val res: Resources) {

    fun getConditionIcon(condition: String?, time: Calendar? = null): Drawable =
            condition?.let {
                val parsedCondition = GenericCondition.parseFromString(condition, time)
                Log.d("ConditionParser", "Parsed condition: $parsedCondition")
                parsedCondition.visibility?.takeIf { it.cause != VisibilityCause.PRECIPITATION }?.let { (_, _, cause, tendency) ->
                    cause?.let {

                        fun getTendency(tendency: Tendency?) =
                                when (tendency) {
                                    Tendency.DEVELOPING -> R.drawable.condition_fog_increasing
                                    Tendency.DISSIPATING -> R.drawable.condition_fog_decreasing
                                    else -> R.drawable.condition_fog
                                }

                        res.getVectorDrawable(when (it) {
                                                  VisibilityCause.FOG -> getTendency(tendency)
                                                  VisibilityCause.FOG_BANKS -> getTendency(tendency)
                                                  VisibilityCause.FOG_PATCHES -> getTendency(tendency)
                                                  VisibilityCause.SHALLOW_FOG -> R.drawable.condition_mist
                                                  VisibilityCause.MIST -> R.drawable.condition_mist
                                                  VisibilityCause.RISK_OF_FOG -> R.drawable.condition_fog
                                                  VisibilityCause.ICE_FOG -> R.drawable.condition_fog
                                                  VisibilityCause.BLOWING_SNOW -> R.drawable.condition_unknown
                                                  VisibilityCause.DRIFTING_SNOW -> R.drawable.condition_unknown
                                                  VisibilityCause.HAZE -> R.drawable.condition_mist
                                                  VisibilityCause.SMOKE -> R.drawable.condition_unknown
                                                  VisibilityCause.BLOWING_DUST -> R.drawable.condition_unknown
                                                  VisibilityCause.PRECIPITATION -> R.drawable.condition_unknown // Never gets here. Or does it?
                                              })
                    }
                }
                ?: parsedCondition.precipitationChances?.let {
                    it.takeIf(Set<PrecipitationChances.WeightedPrecipitation>::isNotEmpty)?.let { pChances ->

                        pChances.maxBy { it.weight ?: 0.0 }?.type?.let {

                            when (it) {
                                PrecipitationType.RAIN, PrecipitationType.RAIN_SHOWER -> PrecipitationIcons.RAIN_NORMAL
                                PrecipitationType.SNOW, PrecipitationType.FLURRY -> PrecipitationIcons.SNOW_NORMAL
                                PrecipitationType.WET_FLURRY -> PrecipitationIcons.WET_FLURRY
                                PrecipitationType.DRIZZLE -> PrecipitationIcons.DRIZZLE_NORMAL
                                PrecipitationType.FREEZING_RAIN -> PrecipitationIcons.FREEZING_RAIN_NORMAL
                                PrecipitationType.FREEZING_DRIZZLE -> PrecipitationIcons.FREEZING_DRIZZLE_NORMAL
                                PrecipitationType.ICE_PELLET, PrecipitationType.ICE_PELLET_SHOWER -> PrecipitationIcons.ICE_PELLET
                                PrecipitationType.ICE_CRYSTAL -> PrecipitationIcons.ICE_CRYSTAL
                                PrecipitationType.SNOW_PELLET -> PrecipitationIcons.SNOW_PELLET
                                PrecipitationType.SNOW_GRAIN -> PrecipitationIcons.SNOW_GRAIN
                                PrecipitationType.THUNDER -> PrecipitationIcons.NONE
                                PrecipitationType.HAIL -> PrecipitationIcons.RAIN_NORMAL
                                PrecipitationType.PRECIPITATION -> PrecipitationIcons.RAIN_NORMAL
                            }.getDrawable(res,
                                          (pChances.totalChance?.measure?.inUnit(Percentage.Units.PERCENT) ?: 100.0) < 100.0 ||
                                          pChances.thunderRisk == RiskLevel.RISK,
                                          pChances.thunderRisk != RiskLevel.NONE)
                        }
                    }
                    ?: when (it.thunderRisk) {
                        RiskLevel.NONE -> null
                        RiskLevel.RISK -> PrecipitationIcons.NONE.getDrawable(res, true, true)
                        RiskLevel.CERTAIN -> PrecipitationIcons.NONE.getDrawable(res, false, true)
                    }
                }
                ?: parsedCondition.cloudiness?.measure?.let {

                    fun getTendency(tendency: Tendency?, default: Int) =
                            when (tendency) {
                                Tendency.INCREASING -> R.drawable.condition_cloudy_increasing
                                Tendency.DECREASING, Tendency.CLEARING -> R.drawable.condition_cloudy_decreasing
                                else -> default
                            }

                    res.getVectorDrawable(getTendency(parsedCondition.cloudiness.tendency,
                                                      when (it.inUnit(Percentage.Units.EIGHTHS)) {
                                                          in 0.0..0.5 -> R.drawable.condition_sunny
                                                          in 0.5..2.5 -> R.drawable.condition_sunny_mostly
                                                          in 2.5..4.5 -> R.drawable.condition_cloudy_partly
                                                          in 4.5..7.5 -> R.drawable.condition_cloudy_mostly
                                                          else -> R.drawable.condition_cloudy
                                                      }))
                }

            } ?: res.getVectorDrawable(R.drawable.condition_unknown)

    enum class PrecipitationIcons(val baseIcon: Int = R.drawable.condition_unknown,
                                  val thunderIcon: Int = R.drawable.condition_unknown) {

        RAIN_LIGHT(R.drawable.condition_rain_light, R.drawable.condition_thunderstorm_rain_light),
        RAIN_NORMAL(R.drawable.condition_rain, R.drawable.condition_thunderstorm_rain),
        RAIN_HEAVY(R.drawable.condition_rain_heavy, R.drawable.condition_thunderstorm_rain_heavy),

        SNOW_LIGHT(R.drawable.condition_snow_light),
        SNOW_NORMAL(R.drawable.condition_snow),
        SNOW_HEAVY(R.drawable.condition_snow_heavy),

        WET_FLURRY,

        DRIZZLE_LIGHT(R.drawable.condition_drizzle_light),
        DRIZZLE_NORMAL(R.drawable.condition_drizzle),
        DRIZZLE_HEAVY(R.drawable.condition_drizzle_heavy),

        FREEZING_RAIN_LIGHT(R.drawable.condition_freezing_rain_light),
        FREEZING_RAIN_NORMAL(R.drawable.condition_freezing_rain),
        FREEZING_RAIN_HEAVY(R.drawable.condition_freezing_rain_heavy),

        FREEZING_DRIZZLE_LIGHT(R.drawable.condition_freezing_drizzle_light),
        FREEZING_DRIZZLE_NORMAL(R.drawable.condition_freezing_drizzle),
        FREEZING_DRIZZLE_HEAVY(R.drawable.condition_freezing_drizzle_heavy),

        ICE_PELLET,
        ICE_CRYSTAL,
        SNOW_PELLET,
        SNOW_GRAIN,
        HAIL(R.drawable.condition_hail, R.drawable.condition_thunderstorm_hail),

        NONE(R.drawable.condition_cloudy, R.drawable.condition_thunderstorm);

        fun getDrawable(resources: Resources, chance: Boolean, thunder: Boolean): Drawable =
                if (chance) {
                    ChancePrecipitationDrawable(resources, resources.getVectorDrawable(if (thunder) thunderIcon else baseIcon))
                }
                else {
                    resources.getVectorDrawable(if (thunder) thunderIcon else baseIcon)
                }
    }
}