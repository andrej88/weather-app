package com.andrej.weatherapp

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.TextView
import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.providers.ProviderGroup
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.support.v4.viewPager

class MainActivity : AppCompatActivity() {

    private var provider = ProviderGroup.canadaProvider

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        coordinatorLayout {
            appBarLayout {

                setTheme(R.style.AppTheme_AppBarOverlay)

                setSupportActionBar(toolbar {
                    popupTheme = R.style.AppTheme_PopupOverlay
                }.apply {
                    // Using .lparams{} causes the menu buttons to be left-aligned, so it has to be done like this.
                    (layoutParams as AppBarLayout.LayoutParams).scrollFlags = 0
                })

                tabLayout = tabLayout()

            }.lparams(width = matchParent)


            viewPager = viewPager {
                // This needs to have an ID or the app will crash
                id = R.id.view_pager
                adapter = SectionsPagerAdapter(supportFragmentManager)
            }.lparams {
                // Prevents the appbar from covering up the content.
                behavior = AppBarLayout.ScrollingViewBehavior(this@MainActivity, null)
            }

            tabLayout.setupWithViewPager(viewPager)
            tabLayout.run {
                getTabAt(0)?.setIcon(R.drawable.tab_current_conditions)?.text = ""
                getTabAt(1)?.setIcon(R.drawable.tab_forecast_calendar)?.text = ""
                getTabAt(2)?.setIcon(R.drawable.tab_forecast_clock)?.text = ""
                getTabAt(3)?.setIcon(R.drawable.tab_fish)?.text = ""
                getTabAt(4)?.setIcon(R.drawable.tab_night)?.text = ""
                getTabAt(5)?.setIcon(R.drawable.tab_sunset)?.text = ""
                getTabAt(6)?.setIcon(R.drawable.tab_radar)?.text = ""
            }
        }


    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        when (id) {
            R.id.action_settings -> return true
            R.id.action_refresh -> {
                // If this doesn't work, try the following: http://stackoverflow.com/a/18611036
                (supportFragmentManager.fragments[viewPager.currentItem] as WeatherFragment).refreshData(true)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    class PlaceholderFragment : WeatherFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.fragment_main, container, false)
            (rootView.findViewById(R.id.section_label) as TextView).text = "@"
            return rootView
        }

        override fun refreshData(userTriggered: Boolean) = Unit

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {

            val location = LatLong(49.264801, -123.252795)

            return when (position) {
                0 -> {
                    val weatherFragment = CurrentWeatherFragment(location)
                    weatherFragment.provider = provider.currentWeatherProvider!!
                    weatherFragment
                }
                1 -> {
                    val dailyForecastFragment = DailyForecastFragment(location)
                    dailyForecastFragment.provider = provider.dailyForecastProvider!!
                    dailyForecastFragment
                }
                2-> {
                    val hourlyForecastFragment = HourlyForecastFragment(location)
                    hourlyForecastFragment.provider = provider.nowcastingProvider!!
                    hourlyForecastFragment
                }
                else -> PlaceholderFragment()
            }
        }

        override fun getCount(): Int {
            return 7
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "Current Weather"
                1 -> return "Daily Forecast"
                2 -> return "Forecast History"
                3 -> return "Ocean"
                4 -> return "Sky"
                5 -> return "Sun & Moon"
                6 -> return "Maps"
            }
            return null
        }
    }
}
