package com.andrej.weatherapp

import android.os.Bundle
import android.view.*
import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.properties.ScalarQuantities.Temperature
import com.andrej.weatherapp.properties.TemperatureProperty
import com.andrej.weatherapp.providers.ForecastProvider
import com.andrej.weatherapp.util.imageViewCompat
import com.andrej.weatherapp.util.lineChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import org.apache.commons.lang3.text.WordUtils
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachWithIndex

class DailyForecastFragment() : ForecastFragmentBase() {

    lateinit private var parser: ConditionParser

    lateinit override var location: LatLong

    lateinit var provider: ForecastProvider
    private val providerKey: String = ForecastProvider::class.java.canonicalName

    private var graphLowColor = 0
    private var graphHighColor = 0
    lateinit private var graphHighTitle: String
    lateinit private var graphLowTitle: String
    lateinit private var temperatureGraph: LineChart


    constructor(location: LatLong) : this() {
        this.location = location
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            provider = savedInstanceState.get(providerKey) as ForecastProvider
            location = savedInstanceState.get(locationKey) as LatLong
        }
        parser = ConditionParser(resources)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        @Suppress("DEPRECATION")
        graphLowColor = resources.getColor(R.color.graph_temp_low)

        @Suppress("DEPRECATION")
        graphHighColor = resources.getColor(R.color.graph_temp_high)

        graphHighTitle = resources.getString(R.string.temperature_hl_graph_high_title)
        graphLowTitle = resources.getString(R.string.temperature_hl_graph_low_title)

        refreshData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(providerKey, provider)
        outState.putParcelable(locationKey, location)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = ForecastFragmentUi().createView(AnkoContext.create(activity, this))


    override fun refreshData(userTriggered: Boolean) {

//        val temperatureGraph = find<LineChart>(R.id.temperature_hl_graph)

        showLoading()

        doAsync {
            val data = provider.acquire(location)
            uiThread {

                forecastList.removeAllViews()

                val highTemps = mutableListOf<Entry>()
                val lowTemps = mutableListOf<Entry>()

                data.forecasts.forEachWithIndex { index, (period, textSummary, _, temperatures) ->
                    with(forecastList) {
                        linearLayout {

                            lparams {
                                topMargin = dip(8f)
                                bottomMargin = dip(8f)
                                leftMargin = dip(8f)
                                rightMargin = dip(8f)
                            }

                            imageViewCompat {
                                image = parser.getConditionIcon(textSummary?.longest)
                            }.lparams {
                                width = dip(48f)
                                height = dip(48f)
                                rightMargin = dip(8f)
                                gravity = Gravity.CENTER_VERTICAL
                            }

                            verticalLayout {

                                textView {
                                    text = WordUtils.capitalize(period?.day?.dayName)
                                    textSize = sp(16f).toFloat()

                                    @Suppress("DEPRECATION")
                                    setTextColor(resources.getColor(R.color.textLight))
                                }

                                textView {
                                    text = textSummary?.longest
                                    textSize = sp(16f).toFloat()

                                    @Suppress("DEPRECATION")
                                    setTextColor(resources.getColor(R.color.textLight))
                                }
                            }
                        }

                        verticalLayout {
                            lparams {
                                width = matchParent
                                height = dip(1f)
                            }

                            @Suppress("DEPRECATION")
                            backgroundColor = resources.getColor(R.color.divider)
                        }
                    }

                    val temperatureHigh = temperatures?.temperatures?.find {
                        it.type == TemperatureProperty.Type.HIGH || it.type == TemperatureProperty.Type.MAX
                    }

                    val temperatureLow = temperatures?.temperatures?.find {
                        it.type == TemperatureProperty.Type.LOW || it.type == TemperatureProperty.Type.MIN
                    }

                    temperatureHigh?.temperature?.let {
                        highTemps.add(Entry(index.toFloat(),
                                            (it inUnit Temperature.Units.CELSIUS).toFloat()))
                    }

                    temperatureLow?.temperature?.let {
                        lowTemps.add(Entry(index.toFloat(),
                                           (it inUnit Temperature.Units.CELSIUS).toFloat()))
                    }
                }


                val highSet = LineDataSet(highTemps, graphHighTitle)
                with(highSet) {
                    color = graphHighColor
                    setDrawCircleHole(false)
                    setCircleColor(graphHighColor)

                    @Suppress("DEPRECATION")
                    valueTextColor = resources.getColor(R.color.textLight)
                }


                val lowSet = LineDataSet(lowTemps, graphLowTitle)
                with(lowSet) {
                    color = graphLowColor
                    setDrawCircleHole(false)
                    setCircleColor(graphLowColor)

                    @Suppress("DEPRECATION")
                    valueTextColor = resources.getColor(R.color.textLight)
                }


                val lineData = LineData(highSet, lowSet)
                temperatureGraph.data = lineData
                temperatureGraph.invalidate()

                hideLoading()

            }
        }

    }


    override fun createForecastList(viewManager: ViewManager) =
            viewManager.verticalLayout {
                lparams(width = matchParent, height = wrapContent)
            }


    override fun createGraphs(viewManager: ViewManager) =
            viewManager.verticalLayout {

                temperatureGraph = lineChart {
                    setNoDataText(resources.getString(R.string.temperature_hl_graph_no_data))
                    setTouchEnabled(false)
                    axisLeft.setDrawZeroLine(true)
                }.lparams {
                    width = matchParent
                    height = dip(200f)
                    leftMargin = dip(8f)
                    rightMargin = dip(8f)
                }

                lparams(width = matchParent, height = wrapContent)
            }
}