package com.andrej.weatherapp

import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.View
import android.view.ViewManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.andrej.weatherapp.properties.LatLong
import com.andrej.weatherapp.util.imageViewCompat
import org.jetbrains.anko.*

abstract class ForecastFragmentBase : WeatherFragment() {

    abstract protected var location: LatLong
    protected val locationKey: String = LatLong::class.java.canonicalName

    protected var graphLineThickness = 0f

    protected lateinit var progressBar: ProgressBar
    protected lateinit var buttons: LinearLayout
    protected lateinit var forecastList: LinearLayout
    protected lateinit var forecastGraphs: LinearLayout
    protected lateinit var buttonList: ImageView
    protected lateinit var buttonGraph: ImageView

    private var currentDisplay = CurrentDisplay.LIST


    private fun showForecastList() {
        currentDisplay = CurrentDisplay.LIST
        buttonList.alpha = 1.0f
        buttonGraph.alpha = 0.54f
        forecastList.visibility = View.VISIBLE
        forecastGraphs.visibility = View.GONE
    }

    private fun showForecastGraphs() {
        currentDisplay = CurrentDisplay.GRAPHS
        buttonList.alpha = 0.54f
        buttonGraph.alpha = 1.0f
        forecastList.visibility = View.GONE
        forecastGraphs.visibility = View.VISIBLE
    }

    protected fun showLoading() {
        progressBar.visibility = View.VISIBLE
        buttons.visibility = View.GONE
        forecastList.visibility = View.GONE
        forecastGraphs.visibility = View.GONE
    }

    protected fun hideLoading() {
        progressBar.visibility = View.GONE
        buttons.visibility = View.VISIBLE
        when (currentDisplay) {
            CurrentDisplay.LIST -> showForecastList()
            CurrentDisplay.GRAPHS -> showForecastGraphs()
        }
    }


    abstract protected fun createForecastList(viewManager: ViewManager): LinearLayout

    abstract protected fun createGraphs(viewManager: ViewManager): LinearLayout


    protected inner class ForecastFragmentUi : AnkoComponent<Fragment> {

        private val buttonSize = 48f

        override fun createView(ui: AnkoContext<Fragment>): View = with(ui) {

            // dip() doesn't like being called elsewhere
            graphLineThickness = dip(3f).toFloat()


            scrollView {
                lparams {
                    height = wrapContent
                    width = matchParent
                }

                verticalLayout {

                    progressBar = progressBar {
                        visibility = View.GONE
                    }.lparams(width = dip(100f), height = dip(100f)) {
                        topMargin = dip(50f)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }

                    buttons = linearLayout {

                        buttonList = imageViewCompat {
                            imageResource = R.drawable.button_list
                            setOnClickListener { showForecastList() }
                        }.lparams {
                            width = matchParent
                            height = dip(buttonSize)
                            weight = 1f
                        }

                        buttonGraph = imageViewCompat {
                            imageResource = R.drawable.button_graph
                            setOnClickListener { showForecastGraphs() }
                        }.lparams {
                            width = matchParent
                            height = dip(buttonSize)
                            weight = 1f
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        topMargin = dip(16f)
                        bottomMargin = dip(16f)
                    }

                    forecastList = createForecastList(this)

                    forecastGraphs = createGraphs(this)

                }
            }
        }
    }

    private enum class CurrentDisplay {
        LIST, GRAPHS
    }
}